var searchData=
[
  ['d',['D',['../class_legyen_on_is_milliomos_1_1_audience_columns.html#a10d9bc43caa3ac490fcd420e5a36b965',1,'LegyenOnIsMilliomos.AudienceColumns.D()'],['../class_legyen_on_is_milliomos_1_1_question.html#afe42b6ea7e6f5e243c3c92f246aae196',1,'LegyenOnIsMilliomos.Question.D()']]],
  ['dbackgroundbrush',['DBackgroundBrush',['../class_legyen_on_is_milliomos_1_1_my_background.html#aeeb28ecd83c9b5960813c0d30a6f2452',1,'LegyenOnIsMilliomos::MyBackground']]],
  ['decrease',['Decrease',['../class_legyen_on_is_milliomos_1_1_time_column.html#a7cd5bc15ad6d165e853fd1c22e1a3493',1,'LegyenOnIsMilliomos::TimeColumn']]],
  ['difficulty',['Difficulty',['../class_legyen_on_is_milliomos_1_1_question.html#ac4fa398e27c397fb1229b1d61f493d25',1,'LegyenOnIsMilliomos::Question']]],
  ['dispmenustring',['DispMenuString',['../class_legyen_on_is_milliomos_1_1_view_model.html#abe47302a1260292b07f9f02f08f5a77b',1,'LegyenOnIsMilliomos::ViewModel']]],
  ['dispnextquestion',['DispNextQuestion',['../class_legyen_on_is_milliomos_1_1_view_model.html#a7c769183d51a3e5ec42f3b16604b1938',1,'LegyenOnIsMilliomos::ViewModel']]],
  ['dispremovedanswersbackground',['DispRemovedAnswersBackground',['../class_legyen_on_is_milliomos_1_1_view_model.html#adb98e0847deeb2c6468f0fd092d5d58b',1,'LegyenOnIsMilliomos::ViewModel']]],
  ['doubletipp',['Doubletipp',['../class_legyen_on_is_milliomos_1_1_game_logic.html#aab6953f52ee70059335b60d55d08760e',1,'LegyenOnIsMilliomos.GameLogic.Doubletipp()'],['../class_legyen_on_is_milliomos_1_1_my_background.html#abbea40e05f9149ea4cacf342fbdf5e28',1,'LegyenOnIsMilliomos.MyBackground.Doubletipp()'],['../namespace_legyen_on_is_milliomos.html#a4122554418adda080a8e0c5ba6521f7aa2551cc87b996a63b0701ceedefa7dde7',1,'LegyenOnIsMilliomos.Doubletipp()']]],
  ['doubletippbrush',['DoubletippBrush',['../class_legyen_on_is_milliomos_1_1_my_background.html#a96ad9844520f7915480509d86bcfe068',1,'LegyenOnIsMilliomos::MyBackground']]],
  ['doubletippcount',['DoubletippCount',['../class_legyen_on_is_milliomos_1_1_game_logic.html#ad2add62e28e70cf5a88969f12c7259ce',1,'LegyenOnIsMilliomos::GameLogic']]],
  ['dtstar',['DTStar',['../class_legyen_on_is_milliomos_1_1_main_window.html#af7e5e00a2afedaafdb6ceecd600ab036',1,'LegyenOnIsMilliomos::MainWindow']]],
  ['dttimecolumn',['DTTimeColumn',['../class_legyen_on_is_milliomos_1_1_main_window.html#a34806dfbc702bca51051eab25c96ce67',1,'LegyenOnIsMilliomos::MainWindow']]]
];
