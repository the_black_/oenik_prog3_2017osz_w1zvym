var searchData=
[
  ['phone',['Phone',['../class_legyen_on_is_milliomos_1_1_game_logic.html#a5b3c41792d02dd8701f3107fe78e3375',1,'LegyenOnIsMilliomos.GameLogic.Phone()'],['../class_legyen_on_is_milliomos_1_1_my_background.html#a601761cbe08f21a02d1f273cf27732ef',1,'LegyenOnIsMilliomos.MyBackground.Phone()']]],
  ['phonebrush',['PhoneBrush',['../class_legyen_on_is_milliomos_1_1_my_background.html#a1ca252827e80c1edc80e3e79e4918f33',1,'LegyenOnIsMilliomos::MyBackground']]],
  ['phonetipp',['PhoneTipp',['../namespace_legyen_on_is_milliomos.html#a3c31e9b2236028a47b5d677b5cb2dd48ad20767d8cf39078ea391cdd48d78ef85',1,'LegyenOnIsMilliomos']]],
  ['phonetippbackground',['PhoneTippBackground',['../class_legyen_on_is_milliomos_1_1_my_background.html#aa989024f870a3246bfa03cf9c4a2b2e1',1,'LegyenOnIsMilliomos::MyBackground']]],
  ['player',['Player',['../class_legyen_on_is_milliomos_1_1_player.html',1,'LegyenOnIsMilliomos.Player'],['../class_legyen_on_is_milliomos_1_1_view_model.html#aaa6ee4d182b099fcf9035f7e034a7b20',1,'LegyenOnIsMilliomos.ViewModel.Player()'],['../class_legyen_on_is_milliomos_1_1_player.html#a150cf93f272fd4b3e5944a4c7d14efea',1,'LegyenOnIsMilliomos.Player.Player(string name, int money)'],['../class_legyen_on_is_milliomos_1_1_player.html#aac85128f03da7e59b4db7c93450b8467',1,'LegyenOnIsMilliomos.Player.Player()']]],
  ['playeranswer2letter',['PlayerAnswer2Letter',['../class_legyen_on_is_milliomos_1_1_player.html#aaf96fac1fc89baad8184182dd2fc75b4',1,'LegyenOnIsMilliomos::Player']]],
  ['playeranswerletter',['Playeranswerletter',['../class_legyen_on_is_milliomos_1_1_player.html#aa86e92042b401a961926923ed958c9dc',1,'LegyenOnIsMilliomos::Player']]],
  ['playerclickedonanser',['PlayerClickedOnAnser',['../class_legyen_on_is_milliomos_1_1_game_logic.html#abb2ec113590c2def5adfb5f28f186ec3',1,'LegyenOnIsMilliomos::GameLogic']]],
  ['playerclickedonhelp',['PlayerClickedOnHelp',['../class_legyen_on_is_milliomos_1_1_game_logic.html#a749424d300375e91a65b8df7e648dd9d',1,'LegyenOnIsMilliomos::GameLogic']]],
  ['playersetup',['PlayerSetup',['../class_legyen_on_is_milliomos_1_1_player_setup.html',1,'LegyenOnIsMilliomos.PlayerSetup'],['../class_legyen_on_is_milliomos_1_1_player_setup.html#a77e88661fdbafd46a96303915b316c9c',1,'LegyenOnIsMilliomos.PlayerSetup.PlayerSetup()']]],
  ['printhelps',['PrintHelps',['../class_legyen_on_is_milliomos_1_1_screen.html#a5c80e4dc2e43df263471864e4869edf4',1,'LegyenOnIsMilliomos::Screen']]],
  ['printtext',['PrintText',['../class_legyen_on_is_milliomos_1_1_end_star.html#a50725ce3538aa0ad8deae7fd513e0e80',1,'LegyenOnIsMilliomos.EndStar.PrintText()'],['../class_legyen_on_is_milliomos_1_1_screen.html#abe9290d2c65fcaa23a87f9ab2f0d7464',1,'LegyenOnIsMilliomos.Screen.PrintText()']]],
  ['propertychanged',['PropertyChanged',['../class_legyen_on_is_milliomos_1_1_bindable.html#a731e93cfbd35205040d8ed941298d8b1',1,'LegyenOnIsMilliomos::Bindable']]],
  ['putgamescreenelements',['PutGameScreenElements',['../class_legyen_on_is_milliomos_1_1_screen.html#a07719c5a3daeac79363f171152fd8d77',1,'LegyenOnIsMilliomos::Screen']]],
  ['putmenuscreenelements',['PutMenuScreenElements',['../class_legyen_on_is_milliomos_1_1_screen.html#af7845e66c7d5a7bffed2bc61d36ec131',1,'LegyenOnIsMilliomos::Screen']]]
];
