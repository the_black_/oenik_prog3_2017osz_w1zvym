﻿//-----------------------------------------------------------------------
// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;
    using System.Windows.Media;

    /// <summary>
    /// A játék mechanikája, itt kerülnek meghatározásra a szabályok.
    /// </summary>
    public class GameLogic
    {
        private static Random rnd = new Random();
        private bool playerClickedOnAnser;
        private bool playerClickedOnHelp;
        private bool? phone;
        private bool? half;
        private bool? audience;
        private bool? doubletipp;
        private int doubletippCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// A játékmódtól függően beállítja a segítségek állapotát.
        /// </summary>
        /// <param name="gl">Az aktuális játékmódot jelöli</param>
        public GameLogic(GameMode gl)
        {
            if (gl == GameMode.Withhelp)
            {
                this.audience = null;
                this.half = null;
                this.phone = null;
                this.doubletipp = false;
            }
            else if (gl == GameMode.Doubletipp)
            {
                this.doubletipp = null;
                this.doubletippCount = 2;
            }
            else
            {
                this.audience = false;
                this.half = false;
                this.phone = false;
                this.doubletipp = false;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether visszadja hogy válaszlehetőségre kattintottunk e.
        /// </summary>
        public bool PlayerClickedOnAnser
        {
            get
            {
                return this.playerClickedOnAnser;
            }

            set
            {
                this.playerClickedOnAnser = value;
            }
        }

        /// <summary>
        /// Gets or sets Telefonos segítség elérhetősége
        /// </summary>
        public bool? Phone
        {
            get
            {
                return this.phone;
            }

            set
            {
                this.phone = value;
            }
        }

        /// <summary>
        /// Gets or sets Felezés segítség elérhetősége
        /// </summary>
        public bool? Half
        {
            get
            {
                return this.half;
            }

            set
            {
                this.half = value;
            }
        }

        /// <summary>
        /// Gets or sets Közönség segítség elérhetősége
        /// </summary>
        public bool? Audience
        {
            get
            {
                return this.audience;
            }

            set
            {
                this.audience = value;
            }
        }

        /// <summary>
        /// Gets or sets Duplatipp segítség elérhetősége
        /// </summary>
        public bool? Doubletipp
        {
            get
            {
                return this.doubletipp;
            }

            set
            {
                this.doubletipp = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether játékos segítségre kattintott e.
        /// </summary>
        public bool PlayerClickedOnHelp
        {
            get
            {
                return this.playerClickedOnHelp;
            }

            set
            {
                this.playerClickedOnHelp = value;
            }
        }

        /// <summary>
        /// Gets or sets duplatitt esetén a megjelölt válaszok számát adja vissza
        /// </summary>
        public int DoubletippCount
        {
            get
            {
                return this.doubletippCount;
            }

            set
            {
                this.doubletippCount = value;
            }
        }

        /// <summary>
        /// Ellenőrzi a játékos által bejelölt válasz helyességét. Elvégzi a színezést és a megfelelő zene lejátszását.
        /// </summary>
        /// <param name="vm">A ViewModelre hivatkozik</param>
        public void CheckAnswer(ViewModel vm)
        {
            if (vm.MenuuLogic.Marker == 1 && vm.GameeLogic.PlayerClickedOnAnser && (ViewModel.Player.GameM != GameMode.Doubletipp || (ViewModel.Player.GameM == GameMode.Doubletipp && vm.GameeLogic.DoubletippCount == 0) || (vm.GameeLogic.Doubletipp == null)))
            {
                // MessageBox.Show("1. " + ViewModel.Player.Playeranswerletter + " 2. " + ViewModel.Player.PlayerAnswer2Letter);
                System.Threading.Thread.Sleep(1000); // ennyi ideig még narancssárga lesz a válasz háttere
                if (vm.ActualQuestion.Answer == ViewModel.Player.Playeranswerletter || vm.ActualQuestion.Answer == ViewModel.Player.PlayerAnswer2Letter)
                {
                    Processing.PlaySong("good_answer.mp3");
                    ViewModel.Player.Questionwon = true;
                    if (ViewModel.Player.Level == 15)
                    {
                        ViewModel.Player.Gameover = true;
                    }

                    if (vm.ActualQuestion.Answer == ViewModel.Player.Playeranswerletter)
                    {
                        vm.BG.ColorAnswer(ViewModel.Player.Playeranswerletter, Colors.Green);
                    }
                    else
                    {
                        vm.BG.ColorAnswer(ViewModel.Player.PlayerAnswer2Letter, Colors.Green);
                    }
                }
                else
                {
                    Processing.PlaySong("wrong_answer.mp3");
                    ViewModel.Player.Gameover = true; // hibásan válaszolt így vesztett
                    ViewModel.Player.Questionwon = false; // nem nyerte meg a kérdést
                    vm.BG.ColorAnswer(ViewModel.Player.Playeranswerletter, Colors.Red);
                    if (ViewModel.Player.PlayerAnswer2Letter != '\0')
                    {
                        vm.BG.ColorAnswer(ViewModel.Player.PlayerAnswer2Letter, Colors.Red);
                    }

                    vm.BG.ColorAnswer(vm.ActualQuestion.Answer, Colors.Green);
                }

                if (this.doubletipp == false)
                {
                    ViewModel.Player.PlayerAnswer2Letter = '\0';
                }
            }

            this.playerClickedOnAnser = false;
        }

        /// <summary>
        /// A következő kérdés előtti teendőket végzi el (beállítja a pénzt, mozgatja a nyereményjelölőt, vezérli az időjelző oszlopot)
        /// </summary>
        /// <param name="vm">A ViewModelre hivatkozik</param>
        /// <param name="main">A main windowsra hivatkozik</param>
        public void NextQuestion(ViewModel vm, MainWindow main)
        {
            if (ViewModel.Player.Questionwon && !ViewModel.Player.Gameover && ViewModel.Player.Level < 16)
            {
                System.Threading.Thread.Sleep(2000);
                vm.SetAllMoney(ViewModel.Player.Level); // beállítja a játékos pénzeit
                ViewModel.Player.Level++; // növeljük a szintjét
                vm.ActualLvlQuestionGenerator(ViewModel.Player.Level); // random kérdést kap a szintnek megfelelően
                if (this.playerClickedOnHelp)
                {
                    vm.Screen.AfterHelp(vm);
                    this.playerClickedOnHelp = false;
                }

                vm.DispNextQuestion();
                main.DTTimeColumn.Start();
                if (ViewModel.Player.Level < 16)
                {
                    vm.BG.MooveMoneyBar();
                }

                if (ViewModel.Player.Level == 16 && ViewModel.Player.Questionwon)
                {
                    ViewModel.Player.Gameover = true;
                }
            }

            if (ViewModel.Player.GameM != GameMode.Suremoney && ViewModel.Player.Gameover && !ViewModel.Player.StopGame && ViewModel.Player.Level != 15)
            {
                ViewModel.Player.Money = 0;
            }

            if (ViewModel.Player.Gameover)
            {
                System.Threading.Thread.Sleep(2000);
                if (ViewModel.Player.Questionwon && ViewModel.Player.Level == 15)
                {
                    vm.SetAllMoney(ViewModel.Player.Level);
                }
                else if (ViewModel.Player.StopGame)
                {
                     vm.SetAllMoney(ViewModel.Player.Level - 1);
                }
                else
                {
                    ViewModel.Player.Money = ViewModel.Player.GuaranteedMoney;
                }

                Processing.WriteTopPlayer(ViewModel.Player);
                vm.MenuuLogic.Marker = -1;
                main.d_group.Children.Clear();
                main.d_group.Children.Add(main.star_back);
                main.d_group.Children.Add(main.star);
                Processing.PlaySong("final.mp3");
                main.DTStar.Start();
                main.DTTimeColumn.Stop();
            }
        }

        /// <summary>
        /// Megvalósítja a felezést, eltűntet két rossz válaszlehetőséget.
        /// </summary>
        /// <param name="vm">A ViewModelre hivatkozik</param>
        public void HelpHalf(ViewModel vm)
        {
            char[] letters = { 'A', 'B', 'C', 'D' };
            char first;
            char second;
            do
            {
                first = letters[rnd.Next(0, 4)];
                second = letters[rnd.Next(0, 4)];
            }
            while (vm.ActualQuestion.Answer == first || vm.ActualQuestion.Answer == second || first == second);
            vm.RemoveAnswer(first);
            vm.RemoveAnswer(second);
        }

        /// <summary>
        /// Generál egy random választ de figyelembe veszi a kérdés nehézségét is.
        /// </summary>
        /// <param name="vm">A ViewModelre hivatkozik</param>
        public void HelpPhone(ViewModel vm)
        {
            int chance = rnd.Next(1, 101);
            char[] all = { 'A', 'B', 'C', 'D' };
            string first_part;
            char second_part;
            if (ViewModel.Player.Level < 5)
            {
                first_part = "Szerintem ";
                if (chance < 80)
                {
                    second_part = vm.ActualQuestion.Answer;
                }
                else
                {
                    second_part = all[rnd.Next(0, 4)];
                }
            }
            else if (ViewModel.Player.Level < 10)
            {
                first_part = "Talán ";
                if (chance < 50)
                {
                    second_part = vm.ActualQuestion.Answer;
                }
                else
                {
                    second_part = all[rnd.Next(0, 4)];
                }
            }
            else
            {
                first_part = "Lehet ";
                if (chance < 50)
                {
                    second_part = vm.ActualQuestion.Answer;
                }
                else
                {
                    second_part = all[rnd.Next(0, 4)];
                }
            }

            vm.Screen.PrintText(first_part + second_part + " .", StringContentEnum.PhoneTipp);
        }

        /// <summary>
        /// Beépített csalás, megváltoztatja a helyes válasz színét.
        /// </summary>
        /// <param name="vm">A ViewModelre hivatkozik</param>
        public void Cheat(ViewModel vm)
        {
            if (vm.MenuuLogic.Marker == 1)
            {
                switch (vm.ActualQuestion.Answer)
                {
                    case 'A': vm.BG.ABackgroundBrush = new SolidColorBrush(Colors.Blue); break;
                    case 'B': vm.BG.BBackgroundBrush = new SolidColorBrush(Colors.Blue); break;
                    case 'C': vm.BG.CBackgroundBrush = new SolidColorBrush(Colors.Blue); break;
                    case 'D': vm.BG.DBackgroundBrush = new SolidColorBrush(Colors.Blue); break;
                }
            }
        }
    }
}
