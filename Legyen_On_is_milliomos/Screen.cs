﻿//-----------------------------------------------------------------------
// <copyright file="Screen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// A kiírandó szöveg típusát mutatja meg.
    /// </summary>
    public enum StringContentEnum
    {
        /// <summary>
        /// krdés típusú string
        /// </summary>
        Question,

        /// <summary>
        /// válasz típusú string
        /// </summary>
        Answer,

        /// <summary>
        /// válasz betűjel típusú string
        /// </summary>
        AnswerLetter,

        /// <summary>
        /// kérdés sorszám típusú string
        /// </summary>
        QuestionNumber,

        /// <summary>
        /// nyeremény összeg típusú string
        /// </summary>
        Money,

        /// <summary>
        /// telefonos segítség típusú string
        /// </summary>
        PhoneTipp,

        /// <summary>
        /// szabálok típusú string
        /// </summary>
        Rules,

        /// <summary>
        /// toplistás típusú string
        /// </summary>
        Top,

        /// <summary>
        /// menü típusú string
        /// </summary>
        MenuString
    }

    /// <summary>
    /// A képernyőn mejelenő tartalmakat vezérlő osztály.
    /// </summary>
    public class Screen
    {
        private int answerCounter = 0; // a válaszlehetőségek poziícióját és azok betűjeleinek pozíóját egy tömbben tárolom, ez a változó azt indexeli
        private int moneyCounter = 1;
        private GeometryDrawing[] questionsAnswers; // azért kell hogy le tudjam törölni a kérdéseket és válaszokat
        private GeometryDrawing[] answerLetterG;
        private Point[] answerPositions; // eltárolom egy tömbben a válaszbetűjelek pozícióját, ebből már kitudom számolni a válasszöveg pozícióját
        private GeometryDrawing[] answerBackgrounds;
        private GeometryDrawing phoneTipp;
        private GeometryDrawing rulesText;
        private GeometryDrawing topPlayersText;
        private GeometryDrawing[] menuStrings;
        private Point[] menuStringPositions;
        private int menuStringCounter;
        private MainWindow main;

        /// <summary>
        /// Initializes a new instance of the <see cref="Screen"/> class.
        /// Beállítja a réglalapok helyeit és létrehozza azokat.
        /// </summary>
        /// <param name="main">Hivatkozás a main windowra.</param>
        public Screen(MainWindow main)
        {
            this.main = main;
            this.questionsAnswers = new GeometryDrawing[6];
            this.answerPositions = new Point[4];
            this.answerPositions[0] = new Point(118, 901); // A
            this.answerPositions[1] = new Point(1055, 901); // B
            this.answerPositions[2] = new Point(118, 973); //  C
            this.answerPositions[3] = new Point(1055, 973); // D
            this.answerLetterG = new GeometryDrawing[4];
            this.answerBackgrounds = new GeometryDrawing[8];
            this.answerBackgrounds[0] = main.aw;
            this.answerBackgrounds[1] = main.ab;
            this.answerBackgrounds[2] = main.bw;
            this.answerBackgrounds[3] = main.bb;
            this.answerBackgrounds[4] = main.cw;
            this.answerBackgrounds[5] = main.cb;
            this.answerBackgrounds[6] = main.dw;
            this.answerBackgrounds[7] = main.db;
            this.rulesText = new GeometryDrawing();
            this.topPlayersText = new GeometryDrawing();
            this.menuStrings = new GeometryDrawing[4];
            this.menuStringPositions = new Point[4];
            this.menuStringPositions[0] = new Point(889, 572);
            this.menuStringPositions[1] = new Point(870, 687);
            this.menuStringPositions[2] = new Point(878, 802);
            this.menuStringPositions[3] = new Point(898, 917);
            this.menuStringCounter = 0;
        }

        /// <summary>
        /// Gets or sets megjelenő válaszok hátterei.
        /// </summary>
        public GeometryDrawing[] AnswerBackgrounds // felezés miatt, meg kell vizsgálni hogy az a válasz amire kattintunk nem-e tűnt már el
        {
            get
            {
                return this.answerBackgrounds;
            }

            set
            {
                this.answerBackgrounds = value;
            }
        }

        /// <summary>
        /// Gets or sets szabályok szövegének geometrydrawingja.
        /// </summary>
        public GeometryDrawing RulesText
        {
            get
            {
                return this.rulesText;
            }

            set
            {
                this.rulesText = value;
            }
        }

        /// <summary>
        /// Gets or sets toplistás játékosok szövegének geometrydrawingja.
        /// </summary>
        public GeometryDrawing TopPlayersText
        {
            get
            {
                return this.topPlayersText;
            }

            set
            {
                this.topPlayersText = value;
            }
        }

        /// <summary>
        /// Gets or sets menüpontok szövegének geometrydrawingja.
        /// </summary>
        public GeometryDrawing[] MenuStrings
        {
            get
            {
                return this.menuStrings;
            }

            set
            {
                this.menuStrings = value;
            }
        }

        /// <summary>
        /// Ezzel a metódussal érhető el az összes szöveg kiíratása a képernyőre.
        /// </summary>
        /// <param name="textToPrint">Kiírandó szöveg</param>
        /// <param name="scEnum">Kiírandó szöveg típusa, ez határozza meg hogy hova kerüljön kiírásra.</param>
        public void PrintText(string textToPrint, StringContentEnum scEnum)
        {
            if (scEnum == StringContentEnum.Question)
            {
                this.PrintTextQuestion(textToPrint);
            }
            else if (scEnum == StringContentEnum.AnswerLetter)
            {
                this.Print_answers_letter(textToPrint);
            }
            else if (scEnum == StringContentEnum.Answer)
            {
                this.Print_answers(textToPrint);
            }
            else if (scEnum == StringContentEnum.Money)
            {
                this.Print_money(textToPrint);
            }
            else if (scEnum == StringContentEnum.PhoneTipp)
            {
                this.Print_phoneTipp(textToPrint);
            }
            else if (scEnum == StringContentEnum.Rules)
            {
                this.Print_rules(textToPrint);
            }
            else if (scEnum == StringContentEnum.Top)
            {
                this.Print_top_players(textToPrint);
            }
            else if (scEnum == StringContentEnum.MenuString)
            {
                this.Print_menu_strings(textToPrint);
            }
        }

        /// <summary>
        /// Magát a menüt jeleníti meg.
        /// </summary>
        public void PutMenuScreenElements()
        {
            this.main.d_group.Children.Add(this.main.menu_background);
            this.main.d_group.Children.Add(this.main.menu_new_game);
            this.main.d_group.Children.Add(this.main.menu_rules);
            this.main.d_group.Children.Add(this.main.menu_rank);
            this.main.d_group.Children.Add(this.main.menu_exit);
        }

        /// <summary>
        /// A magát a jáék képernyőjét jeleníti meg.
        /// </summary>
        public void PutGameScreenElements()
        {
            this.main.d_group.Children.Add(this.main.back1);
            this.main.d_group.Children.Add(this.main.timebo);
            this.main.d_group.Children.Add(this.main.timeba);
            this.main.d_group.Children.Add(this.main.time);
            this.main.d_group.Children.Add(this.main.questionw);
            this.main.d_group.Children.Add(this.main.questionb);
            this.main.d_group.Children.Add(this.main.aw);
            this.main.d_group.Children.Add(this.main.ab);
            this.main.d_group.Children.Add(this.main.bw);
            this.main.d_group.Children.Add(this.main.bb);
            this.main.d_group.Children.Add(this.main.cw);
            this.main.d_group.Children.Add(this.main.cb);
            this.main.d_group.Children.Add(this.main.dw);
            this.main.d_group.Children.Add(this.main.db);
            this.main.d_group.Children.Add(this.main.moneyba);
            this.main.d_group.Children.Add(this.main.moneybo);
            this.main.d_group.Children.Add(this.main.half);
            this.main.d_group.Children.Add(this.main.phone);
            this.main.d_group.Children.Add(this.main.audience);
            this.main.d_group.Children.Add(this.main.exit);
            this.main.d_group.Children.Add(this.main.ft);
            this.main.d_group.Children.Add(this.main.doubletipp);
            this.main.d_group.Children.Add(this.main.audienceBackground);
            this.main.d_group.Children.Add(this.main.audienceA);
            this.main.d_group.Children.Add(this.main.audienceB);
            this.main.d_group.Children.Add(this.main.audienceC);
            this.main.d_group.Children.Add(this.main.audienceD);
            this.main.d_group.Children.Add(this.main.phoneB);
            this.main.d_group.Children.Add(this.main.timebo);
            this.main.d_group.Children.Add(this.main.timeba);
            this.main.d_group.Children.Add(this.main.time);
        }

        /// <summary>
        /// A játlkmód függvényében a segítségeket jeleníti meg.
        /// </summary>
        /// <param name="mode">Az éppen aktuális játékmódot tartalmazza.</param>
        public void PrintHelps(GameMode mode)
        {
            switch (mode)
            {
                case GameMode.Withhelp:
                    this.main.d_group.Children.Remove(this.main.doubletipp);
                    break;
                case GameMode.Suremoney:
                    this.main.d_group.Children.Remove(this.main.half);
                    this.main.d_group.Children.Remove(this.main.phone);
                    this.main.d_group.Children.Remove(this.main.doubletipp);
                    this.main.d_group.Children.Remove(this.main.audience);
                    break;
                case GameMode.Doubletipp:
                    this.main.d_group.Children.Remove(this.main.half);
                    this.main.d_group.Children.Remove(this.main.phone);
                    this.main.d_group.Children.Remove(this.main.audience);
                    break;
            }

            this.main.d_group.Children.Remove(this.main.phoneB);
            this.RemoveAudienceHelpColumns();
        }

        /// <summary>
        /// A kérdéseket és válaszokat törli a képernyőről.
        /// </summary>
        public void RemoveQA()
        {
            foreach (GeometryDrawing item in this.questionsAnswers)
            {
                if (item != null)
                {
                    this.main.d_group.Children.Remove(item);
                }
            }
        }

        /// <summary>
        /// A paraméterül kapott válaszlehetőséget törli a képernyőről, a felezéskor vesszük hasznát.
        /// </summary>
        /// <param name="letter">A törlnedő válaszlehetőség jele.</param>
        public void RemoveAnswer(char letter)
        {
            switch (letter)
            {
                case 'A':
                    this.main.d_group.Children.Remove(this.answerBackgrounds[0]);
                    this.main.d_group.Children.Remove(this.answerBackgrounds[1]);
                    this.main.d_group.Children.Remove(this.questionsAnswers[2]);
                    this.main.d_group.Children.Remove(this.answerLetterG[0]);

                    break;
                case 'B':
                    this.main.d_group.Children.Remove(this.answerBackgrounds[2]);
                    this.main.d_group.Children.Remove(this.answerBackgrounds[3]);
                    this.main.d_group.Children.Remove(this.questionsAnswers[3]);
                    this.main.d_group.Children.Remove(this.answerLetterG[1]);
                    break;
                case 'C':
                    this.main.d_group.Children.Remove(this.answerBackgrounds[4]);
                    this.main.d_group.Children.Remove(this.answerBackgrounds[5]);
                    this.main.d_group.Children.Remove(this.questionsAnswers[4]);
                    this.main.d_group.Children.Remove(this.answerLetterG[2]);
                    break;
                case 'D':
                    this.main.d_group.Children.Remove(this.answerBackgrounds[6]);
                    this.main.d_group.Children.Remove(this.answerBackgrounds[7]);
                    this.main.d_group.Children.Remove(this.questionsAnswers[5]);
                    this.main.d_group.Children.Remove(this.answerLetterG[3]);
                    break;
            }
        }

        /// <summary>
        /// Felezés segítségének elhasználása és a következő kérdés betöltése előtt ismét megjeleníti a válaszlehetőségek háttereit.
        /// </summary>
        public void RedrawAnswerBackgrounds()
        {
            if (!this.main.d_group.Children.Contains(this.main.ab))
            {
                this.main.d_group.Children.Add(this.main.aw);
                this.main.d_group.Children.Add(this.main.ab);
                this.main.d_group.Children.Add(this.answerLetterG[0]);
            }

            if (!this.main.d_group.Children.Contains(this.main.bb))
            {
                this.main.d_group.Children.Add(this.main.bw);
                this.main.d_group.Children.Add(this.main.bb);
                this.main.d_group.Children.Add(this.answerLetterG[1]);
            }

            if (!this.main.d_group.Children.Contains(this.main.cb))
            {
                this.main.d_group.Children.Add(this.main.cw);
                this.main.d_group.Children.Add(this.main.cb);
                this.main.d_group.Children.Add(this.answerLetterG[2]);
            }

            if (!this.main.d_group.Children.Contains(this.main.db))
            {
                this.main.d_group.Children.Add(this.main.dw);
                this.main.d_group.Children.Add(this.main.db);
                this.main.d_group.Children.Add(this.answerLetterG[3]);
            }
        }

        /// <summary>
        /// A közönség segítség oszlopait és annak hátterét távolítja el.
        /// </summary>
        public void RemoveAudienceHelpColumns()
        {
            this.main.d_group.Children.Remove(this.main.audienceBackground);
            this.main.d_group.Children.Remove(this.main.audienceA);
            this.main.d_group.Children.Remove(this.main.audienceB);
            this.main.d_group.Children.Remove(this.main.audienceC);
            this.main.d_group.Children.Remove(this.main.audienceD);
        }

        /// <summary>
        /// Egy segítség elhasználása után leveszi a képernyőről az aktuálisan elhasznált segítséget.
        /// </summary>
        /// <param name="vm">Hivatkozás a ViewModellre.</param>
        public void AfterHelp(ViewModel vm)
        {
            if (vm.GameeLogic.Half == true)
            {
                vm.DispRemovedAnswersBackground();
                vm.GameeLogic.Half = false;
            }

            if (vm.GameeLogic.Audience == true)
            {
                vm.Screen.RemoveAudienceHelpColumns();
                vm.GameeLogic.Audience = false;
            }

            if (vm.GameeLogic.Phone == true)
            {
                this.main.d_group.Children.Remove(this.main.phoneB);
                this.main.d_group.Children.Remove(this.phoneTipp);
                vm.GameeLogic.Phone = false;
            }
        }

        /// <summary>
        /// A paraméterül kapott kérdést szépen eligazítottan megjeleníti.
        /// </summary>
        /// <param name="textToPrint">A kérdés szövege.</param>
        private void PrintTextQuestion(string textToPrint)
        {
            FormattedText text = new FormattedText(textToPrint, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 50, Brushes.Black);
            Console.WriteLine(text.WidthIncludingTrailingWhitespace);
            Geometry geometry;
            Geometry geometry1;
            Geometry geometry2;
            double full_space = 1690;

            // ha két sorba kell a kérdést tördelni
            if (text.WidthIncludingTrailingWhitespace > 1670)
            {
                int space_pcs = 0;
                for (int i = 0; i < textToPrint.Length / 2; i++)
                {
                    if (textToPrint[i] == ' ')
                    {
                        space_pcs++;
                    }
                }

                string[] split = textToPrint.Split(' ');
                string first = string.Empty;
                string second = string.Empty;
                for (int i = 0; i < split.Length - 1; i++)
                {
                    if (i + 1 <= space_pcs)
                    {
                        first += split[i] + " ";
                    }
                    else
                    {
                        second += split[i] + " ";
                    }
                }

                second += split[split.Length - 1];
                FormattedText text1 = new FormattedText(first, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 50, Brushes.Black); // kérdés első fele
                double text_width1 = text1.WidthIncludingTrailingWhitespace;
                double free_space1 = full_space - text_width1;
                double half_free_space1 = free_space1 / 2;
                geometry1 = text1.BuildGeometry(new Point(half_free_space1 + 115, 750));

                FormattedText text2 = new FormattedText(second, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 50, Brushes.Black); // kérdés második fele
                double text_width2 = text2.WidthIncludingTrailingWhitespace;
                double free_space2 = full_space - text_width2;
                double half_free_space2 = free_space2 / 2;
                geometry2 = text2.BuildGeometry(new Point(half_free_space2 + 115, 750 + 61));

                GeometryDrawing g1 = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry1);
                GeometryDrawing g2 = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry2);

                // kiíratás
                this.main.d_group.Children.Add(g1);
                this.main.d_group.Children.Add(g2);

                // mentés a törléshez
                this.questionsAnswers[0] = g1;
                this.questionsAnswers[1] = g2;
            }

            // ha kifér egy sorba
            else
            {
                double text_width = text.WidthIncludingTrailingWhitespace;
                double free_space = full_space - text_width;
                double half_free_space = free_space / 2;

                text = new FormattedText(textToPrint, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 50, Brushes.Black);
                geometry = text.BuildGeometry(new Point(half_free_space + 115, 750 + 25));
                GeometryDrawing g = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry);
                this.main.d_group.Children.Add(g);
                this.questionsAnswers[0] = g;
                this.questionsAnswers[1] = null;
            }
        }

        /// <summary>
        /// A válaszlehetőségek betűjeleit rajzolja ki.
        /// </summary>
        /// <param name="answer_letter">Éppen kiírandó válasz betűjele.</param>
        private void Print_answers_letter(string answer_letter)
        {
            FormattedText text = new FormattedText(answer_letter, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 43, Brushes.Black);
            Geometry geometry = text.BuildGeometry(this.answerPositions[this.answerCounter]);

            GeometryDrawing g = new GeometryDrawing(new SolidColorBrush(MyBackground.Orange), null, geometry);
            this.main.d_group.Children.Add(g);
            this.answerLetterG[this.answerCounter] = g;
            this.answerCounter++;
            if (this.answerCounter == 4)
            {
                this.answerCounter = 0;
            }
        }

        /// <summary>
        /// A válaszlehetőségeket írja ki a képernyőre.
        /// </summary>
        /// <param name="answer">A kiírandó válasz tartalma.</param>
        private void Print_answers(string answer)
        {
            FormattedText text = new FormattedText(answer, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 40, Brushes.Black);
            Geometry geometry = text.BuildGeometry(new Point(this.answerPositions[this.answerCounter].X + 53, this.answerPositions[this.answerCounter].Y - 1));
            GeometryDrawing g = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry);
            this.questionsAnswers[this.answerCounter + 2] = g; // kettővel odébb kell tenni mert az első két eleme a tömbnek az a kérdés
            this.main.d_group.Children.Add(g);
            this.answerCounter++;
            if (this.answerCounter == 4)
            {
                this.answerCounter = 0;
            }
        }

        /// <summary>
        /// A megfelelő módon írja ki a nyereményösszegeket (figyelembe veszi a játékmódot)
        /// </summary>
        /// <param name="money">Az éppen kiírandó nyereményösszeg.</param>
        private void Print_money(string money)
        {
            FormattedText text = new FormattedText(money, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 30, Brushes.Black);
            Geometry geometry = text.BuildGeometry(new Point(1479 + (3 * 7), 722 - (this.moneyCounter * 36) - 7));

            Color money_color = Colors.White;

            // biztos nyeremények más színűek
            if (ViewModel.Player.GameM == GameMode.Suremoney && this.moneyCounter != 1 && (this.moneyCounter % 5 == 0))
            {
                money_color = MyBackground.Orange;
            }

            GeometryDrawing g = new GeometryDrawing(new SolidColorBrush(money_color), null, geometry);
            this.main.d_group.Children.Add(g);
            this.moneyCounter++;
            if (this.moneyCounter == 16)
            {
                this.moneyCounter = 0;
            }
        }

        /// <summary>
        /// A telefonnos segítség tippjét írja ki.
        /// </summary>
        /// <param name="tipp">A telefonos tipp szövege.</param>
        private void Print_phoneTipp(string tipp)
        {
            FormattedText text = new FormattedText(tipp, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 40, Brushes.Black);
            Geometry geometry = text.BuildGeometry(new Point(770, 672));
            this.phoneTipp = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry);
            this.main.d_group.Children.Add(this.phoneTipp);
        }

        /// <summary>
        /// A játékszabályokat írja ki.
        /// </summary>
        /// <param name="textToPrint">A játékszabály tartalma.</param>
        private void Print_rules(string textToPrint)
        {
            FormattedText text = new FormattedText(textToPrint, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 17, Brushes.Black);
            Geometry geometry = text.BuildGeometry(new Point(1235, 563));
            this.rulesText = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry);
            this.main.d_group.Children.Add(this.rulesText);
        }

        /// <summary>
        /// A topplistás játékosokat írja ki.
        /// </summary>
        /// <param name="textToPrint">A topplistás játékosok neveit és nyereményeit tartalmazza. </param>
        private void Print_top_players(string textToPrint)
        {
            FormattedText text = new FormattedText(textToPrint, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 17, Brushes.Black);
            Geometry geometry = text.BuildGeometry(new Point(1235, 563));
            this.topPlayersText = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry);
            this.main.d_group.Children.Add(this.topPlayersText);
        }

        /// <summary>
        /// A menüpontok neveit írja ki.
        /// </summary>
        /// <param name="textToPrint">Az éppen kiírandó menüpont neve.</param>
        private void Print_menu_strings(string textToPrint)
        {
            FormattedText text = new FormattedText(textToPrint, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), 40, Brushes.Black);
            Geometry geometry = text.BuildGeometry(this.menuStringPositions[this.menuStringCounter]);
            GeometryDrawing geo = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry);
            this.menuStrings[this.menuStringCounter] = geo;
            this.main.d_group.Children.Add(geo);
            this.menuStringCounter++;
            if (this.menuStringCounter == 4)
            {
                this.menuStringCounter = 0;
            }
        }
    }
}
