﻿//-----------------------------------------------------------------------
// <copyright file="Processing.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// A fájlműveletekhez kapcsolódó segédosztály.
    /// </summary>
    public static class Processing
    {
        /// <summary>
        /// Zenék lejátszásához szükséges.
        /// </summary>
        private static MediaPlayer mediaPlayer = new MediaPlayer();

        /// <summary>
        /// Gets or sets zenelejátszó.
        /// </summary>
        public static MediaPlayer MediaPlayer
        {
            get
            {
                return mediaPlayer;
            }

            set
            {
                mediaPlayer = value;
            }
        }

        /// <summary>
        /// Beolvassa a kérdéseket a fájlból.
        /// </summary>
        /// <returns>Visszadja a kérdések egyűjteményét.</returns>
        public static ObservableCollection<Question> ReadQuestionsFromFile()
        {
            string[] rows = File.ReadAllLines("text/loim3.txt", Encoding.Unicode);
            ObservableCollection<Question> questions = new ObservableCollection<Question>();
            for (int i = 0; i < rows.Length; i++)
            {
                string[] actual_row = rows[i].Split('\t');
                questions.Add(new Question(int.Parse(actual_row[0]), actual_row[1], actual_row[2], actual_row[3], actual_row[4], actual_row[5], actual_row[6].ToCharArray()[0]));
            }

            return questions;
        }

        /// <summary>
        /// A különböző képek betöltését végzi.
        /// </summary>
        /// <param name="filename">A fájl neve, amit beszeretnénk tölteni.</param>
        /// <returns>Brushként visszaadja a betöltött képet.</returns>
        public static Brush GetBrush(string filename)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(filename, UriKind.Relative)));
            return ib;
        }

        /// <summary>
        /// Beolvassa a nyerényösszegeket.
        /// </summary>
        /// <returns>Egy tömbben visszaadja a nyeremények összegét</returns>
        public static string[] MoneyFromFile()
        {
            return File.ReadAllLines("text/money.txt");
        }

        /// <summary>
        /// Adott kérdésnél strinből intté alakítja az éppen játékos által elvihető összeget.
        /// </summary>
        /// <param name="lvl">Aktuális nyereményszintet jelöli.</param>
        /// <param name="money">A nyereményösszegeket tartalmazó tömb.</param>
        /// <returns>Az éppen elvihető összeget adja vissza.</returns>
        public static int MoneyFromString(int lvl, string[] money)
        {
            string s = string.Empty;
            if (lvl != 0)
            {
                for (int i = 0; i < money[lvl - 1].Length - 3; i++)
                {
                    if (char.IsNumber(money[lvl - 1][i]))
                    {
                        s += money[lvl - 1][i];
                    }
                }
            }
            else
            {
                s = "0";
            }

            return int.Parse(s);
        }

        /// <summary>
        /// Beolvassa a szabályokat, melyet a menüben ellehet olvasni.
        /// </summary>
        /// <returns>A játék szabályzatot adja vissza.</returns>
        public static string ReadRulesTextFromFile()
        {
            string[] input = File.ReadAllLines("text/rules.txt");
            string s = string.Empty;
            for (int i = 0; i < input.Length; i++)
            {
                s += input[i] + "\n";
            }

            return s;
        }

        /// <summary>
        /// Beolvassa a ranglistában megjeleő játékosokat.
        /// </summary>
        /// <returns>A toplistás játékosokat adja vissza.</returns>
        public static string ReadPlayersFromFile()
        {
            StreamReader stream = null;
            string top = string.Empty;
            try
            {
                stream = new StreamReader("text/top_players.txt");
                string s = string.Empty;
                int counter = 0;
                while (!stream.EndOfStream && counter < 20)
                {
                    s = stream.ReadLine();
                    top += s + "\n";
                    counter++;
                }
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }

            return top;
        }

        /// <summary>
        /// Kiírja a fájlba ajátkos nyereményét és nevét, ezáltal bekerülhet a toplistára.
        /// </summary>
        /// <param name="p">A játékost tárolja.</param>
        public static void WriteTopPlayer(Player p)
        {
            StreamReader stream = null;
            Player[] to_sort;
            Player[] top_player;
            int counter = 0;
            try
            {
                stream = new StreamReader("text/top_players.txt");
                string s = string.Empty;
                string top = string.Empty;
                top_player = new Player[19];
                while (!stream.EndOfStream && counter < 20)
                {
                    s = stream.ReadLine();
                    if (counter > 1)
                    {
                        string[] s2 = s.Split('\t');
                        top_player[counter - 2] = new Player(s2[2], int.Parse(s2[5]));
                    }

                    top += s + "\n";
                    counter++;
                }
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }

            top_player[counter - 2] = p;
            if (counter - 2 == 0)
            {
                to_sort = new Player[1];
                to_sort[0] = p;
            }
            else
            {
                to_sort = new Player[counter - 1];
                for (int i = 0; i < counter - 1; i++)
                {
                    to_sort[i] = top_player[i];
                }

                Array.Sort(to_sort);
            }

            int idx = 0;
            List<string> to_write = new List<string>();
            to_write.Add("#		Név			Nyereménye");
            to_write.Add("******************************************************************");
            while (idx < counter - 1 && to_sort[idx] != null && idx < 18)
            {
                to_write.Add((idx + 1) + ". " + to_sort[idx].ToString());
                idx++;
            }

            File.WriteAllLines("text\\top_players.txt", to_write);
        }

        /// <summary>
        /// Lejátsza a paraméterül kapott zenét.
        /// </summary>
        /// <param name="filename">A zene elérési útja.</param>
        public static void PlaySong(string filename)
        {
            mediaPlayer.Open(new Uri(Directory.GetCurrentDirectory() + "\\music\\" + filename, UriKind.Absolute));
            mediaPlayer.Play();
        }

        /// <summary>
        /// Átállítja a fájlok alapértelmezett útvonalját.
        /// </summary>
        public static void SetNewFilePath()
        {
            string oldPath = Directory.GetCurrentDirectory();
            string[] splitted = oldPath.Split('\\');
            string newPath = string.Empty;
            int j = 0;
            for (j = 0; j < splitted.Length - 3; j++)
            {
                newPath += splitted[j] + "\\";
            }

            newPath += splitted[j];
            Directory.SetCurrentDirectory(newPath);
        }
    }
}
