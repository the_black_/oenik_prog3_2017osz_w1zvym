﻿//-----------------------------------------------------------------------
// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Az egész játékot ööszekapcsoló osztály.
    /// </summary>
    public class ViewModel
    {
        private static Random rnd = new Random();
        private static Player player;
        private TimeColumn animatedTimeColumn;
        private AudienceColumns audienceHelp;
        private MyBackground bg;
        private GameLogic gameLogic;
        private MenuLogic menuLogic;
        private ClickManager clickManager;
        private Screen screen;
        private string[] money;
        private Question actualQuestion;
        private ObservableCollection<Question> questions;
        private EndStar star;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// Létrehozza a játék folyamán szükséges osztályokat.
        /// </summary>
        /// <param name="main">Hivatkozás a main windowra.</param>
        public ViewModel(MainWindow main)
        {
            this.bg = new MyBackground();
            this.menuLogic = new MenuLogic();
            this.screen = new Screen(main);
            this.clickManager = new ClickManager();
            player = new Player();
            this.money = new string[15];
            this.money = Processing.MoneyFromFile();
            this.questions = new ObservableCollection<Question>();
            this.questions = Processing.ReadQuestionsFromFile();
            this.audienceHelp = new AudienceColumns();
            this.star = new EndStar(main);
        }

        /// <summary>
        /// Gets or sets játékos.
        /// </summary>
        public static Player Player
        {
            get
            {
                return player;
            }

            set
            {
                player = value;
            }
        }

        /// <summary>
        /// Gets or sets játék logikája.
        /// </summary>
        public GameLogic GameeLogic
        {
            get
            {
                return this.gameLogic;
            }

            set
            {
                this.gameLogic = value;
            }
        }

        /// <summary>
        /// Gets or sets játék főbb háttérereit kezelő osztály.
        /// </summary>
        public MyBackground BG
        {
            get
            {
                return this.bg;
            }

            set
            {
                this.bg = value;
            }
        }

        /// <summary>
        /// Gets or sets a klikkeket kezelő osztály.
        /// </summary>
        public ClickManager ClickkManager
        {
            get
            {
                return this.clickManager;
            }

            set
            {
                this.clickManager = value;
            }
        }

        /// <summary>
        /// Gets or sets közönség segítség oszlopai.
        /// </summary>
        public AudienceColumns AudienceHelp
        {
            get
            {
                return this.audienceHelp;
            }

            set
            {
                this.audienceHelp = value;
            }
        }

        /// <summary>
        /// Gets or sets képernyőn megjelenítést végző osztáéy
        /// </summary>
        public Screen Screen
        {
            get
            {
                return this.screen;
            }

            set
            {
                this.screen = value;
            }
        }

        /// <summary>
        /// Gets or sets menülogika.
        /// </summary>
        public MenuLogic MenuuLogic
        {
            get
            {
                return this.menuLogic;
            }

            set
            {
                this.menuLogic = value;
            }
        }

        /// <summary>
        /// Gets or sets jutalom csillag.
        /// </summary>
        public EndStar Star
        {
            get
            {
                return this.star;
            }

            set
            {
                this.star = value;
            }
        }

        /// <summary>
        /// Gets or sets aktuális kérdést adja vissza.
        /// </summary>
        public Question ActualQuestion
        {
            get
            {
                return this.actualQuestion;
            }

            set
            {
                this.actualQuestion = value;
            }
        }

        /// <summary>
        /// Gets or sets idő oszlop.
        /// </summary>
        public TimeColumn AnimatedTimeColumn
        {
            get
            {
                return this.animatedTimeColumn;
            }

            set
            {
                this.animatedTimeColumn = value;
            }
        }

        /// <summary>
        /// Létrehozza a hátralévő időt megjelenítő időoszlopot.
        /// </summary>
        public void TimeElement()
        {
            this.animatedTimeColumn = new TimeColumn();
        }

        /// <summary>
        /// A következő kérdés megjelenítéséhez kapcsolódó tevékenységeket végzi el.
        /// </summary>
        public void DispNextQuestion()
        {
            this.RemoveQuestionAnswers();
            this.BG.ResetAnswerBackgroundBrush();
            this.AnimatedTimeColumn.Reset();
            this.DispQA();
        }

        /// <summary>
        /// A válaszlehetőségek eltávolított háttereit jeleníti meg.
        /// </summary>
        public void DispRemovedAnswersBackground()
        {
            this.screen.RedrawAnswerBackgrounds();
        }

        /// <summary>
        /// A menüpontokat jeleníti meg.
        /// </summary>
        public void DispMenuString()
        {
            this.screen.PrintText("Új játék", StringContentEnum.MenuString);
            this.screen.PrintText("Szabályok", StringContentEnum.MenuString);
            this.screen.PrintText("Ranglista", StringContentEnum.MenuString);
            this.screen.PrintText("Kilépés", StringContentEnum.MenuString);
        }

        /// <summary>
        /// A éppen aktuális szinthez tartozó kérdést generálja le.
        /// </summary>
        /// <param name="lvl">Az aktuális szintet jelöli</param>
        public void ActualLvlQuestionGenerator(int lvl)
        {
            player.Questionwon = false;
            if (lvl <= 15)
            {
                int start_idx;
                int end_idx;
                int left = 0; // logaritmikus keresés indítása
                int right = this.questions.Count - 1;
                int center = (left + right) / 2;
                while (left <= right && this.questions[center].Difficulty != lvl)
                {
                    if (this.questions[center].Difficulty > lvl)
                    {
                        right = center - 1;
                    }
                    else
                    {
                        left = center + 1;
                    }

                    center = (left + right) / 2;
                }

                start_idx = center;

                // első előfordulási hely keresése
                while (start_idx > 0 && this.questions[start_idx - 1].Difficulty == lvl)
                {
                    start_idx--;
                }

                end_idx = start_idx;

                // utolsó előfordulási hely keresése
                while (end_idx < this.questions.Count && this.questions[end_idx].Difficulty == lvl)
                {
                    end_idx++;
                }

                end_idx--;
                Question q = this.questions[rnd.Next(start_idx, end_idx + 1)];
                this.actualQuestion = q;
            }
        } // random kérdést generál a megadott szinthez

        /// <summary>
        /// Beállítja a játkos pénzét a szintnek megfelelően.
        /// </summary>
        /// <param name="lvl">Az aktuális szint.</param>
        public void SetAllMoney(int lvl)
        {
            this.SetGuaranteedMoney(lvl); // ha olyan játékot játszik hogy nincs garantált összeg akkor nem állítódik ilyen be
            player.Money = Processing.MoneyFromString(lvl, this.money); // szintnövelés előtt állítódik be
        }

        /// <summary>
        /// A paraméterül kapott válaszlehetőséget távolítja el.
        /// </summary>
        /// <param name="letter">Válaszlehetőség betűjele.</param>
        public void RemoveAnswer(char letter)
        {
            this.screen.RemoveAnswer(letter);
        }

        /// <summary>
        /// A játék elindulását segíti
        /// </summary>
        public void Start()
        {
            this.Disp_hepls();
            this.ActualLvlQuestionGenerator(player.Level);
            this.DispAnswerLetter();
            this.DispMoneyValue();
            this.DispQA();
            this.gameLogic = new GameLogic(player.GameM);
        }

        /// <summary>
        /// Kiírja a nyeremények összegeit a játékképernyőjére.
        /// </summary>
        private void DispMoneyValue()
        {
            for (int i = 0; i < 15; i++)
            {
                this.screen.PrintText((i + 1) + ". " + this.money[i], StringContentEnum.Money);
            }
        }

        /// <summary>
        /// Megjeleníti a válaszlehetőségek betűjeleit.
        /// </summary>
        private void DispAnswerLetter()
        {
            this.screen.PrintText("A: ", StringContentEnum.AnswerLetter);
            this.screen.PrintText("B: ", StringContentEnum.AnswerLetter);
            this.screen.PrintText("C: ", StringContentEnum.AnswerLetter);
            this.screen.PrintText("D: ", StringContentEnum.AnswerLetter);
        } // válaszlehetőségek betűjelét írja ki

        /// <summary>
        /// A kérdés szövegét és a válaszok tartalmát írja ki.
        /// </summary>
        private void DispQA()
        {
            this.screen.PrintText(this.actualQuestion.Difficulty + ". " + this.actualQuestion.Content, StringContentEnum.Question);

            this.screen.PrintText(this.actualQuestion.A, StringContentEnum.Answer);
            this.screen.PrintText(this.actualQuestion.B, StringContentEnum.Answer);
            this.screen.PrintText(this.actualQuestion.C, StringContentEnum.Answer);
            this.screen.PrintText(this.actualQuestion.D, StringContentEnum.Answer);
        }

        /// <summary>
        /// játékmódnak megfelelő segítségeket írja ki.
        /// </summary>
        private void Disp_hepls()
        {
            this.screen.PrintHelps(player.GameM);
        }

        /// <summary>
        /// A garantált nyereményt állítja be.
        /// </summary>
        /// <param name="lvl">Az aktuális szintet jelöli.</param>
        private void SetGuaranteedMoney(int lvl)
        {
            if (player.GameM == GameMode.Suremoney && lvl > 1 && lvl % 5 == 0)
            {
                player.GuaranteedMoney = Processing.MoneyFromString(lvl, this.money);
            }
        }

        /// <summary>
        /// Eltávolítja a lépernyőről a kérdést és a hozzá tartozó válaszokat.
        /// </summary>
        private void RemoveQuestionAnswers()
        {
            this.screen.RemoveQA();
        } // eltűnteti a kérdést és a válaszlehetőségeket
    }
}
