﻿//-----------------------------------------------------------------------
// <copyright file="Question.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    /// <summary>
    /// A kérdés és a hozzá tartozó válaszokat és reprezentáló osztály.
    /// </summary>
    public class Question
    {
        private int difficulty;
        private string a;
        private string b;
        private string c;
        private string d;
        private char answer;
        private string content;

        /// <summary>
        /// Initializes a new instance of the <see cref="Question"/> class.
        /// Kérédést hozza létre.
        /// </summary>
        /// <param name="difficulty">kérédés nehézsége, valójában a sorszáma</param>
        /// <param name="content">kérdés tartalma</param>
        /// <param name="a">a válasz</param>
        /// <param name="b">b válasz</param>
        /// <param name="c">c válasz</param>
        /// <param name="d">d válasz</param>
        /// <param name="answer">helyes válasz</param>
        public Question(int difficulty, string content, string a, string b, string c, string d, char answer)
        {
            this.difficulty = difficulty;
            this.content = content;
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.answer = answer;
        }

        /// <summary>
        /// Gets or sets kérdés sorszáma.
        /// </summary>
        public int Difficulty
        {
            get
            {
                return this.difficulty;
            }

            set
            {
                this.difficulty = value;
            }
        }

        /// <summary>
        /// Gets or sets A válasz tartalma.
        /// </summary>
        public string A
        {
            get
            {
                return this.a;
            }

            set
            {
                this.a = value;
            }
        }

        /// <summary>
        /// Gets or sets B válasz tartalma.
        /// </summary>
        public string B
        {
            get
            {
                return this.b;
            }

            set
            {
                this.b = value;
            }
        }

        /// <summary>
        /// Gets or sets C válasz tartalma.
        /// </summary>
        public string C
        {
            get
            {
                return this.c;
            }

            set
            {
                this.c = value;
            }
        }

        /// <summary>
        /// Gets or sets D válasz tartalma.
        /// </summary>
        public string D
        {
            get
            {
                return this.d;
            }

            set
            {
                this.d = value;
            }
        }

        /// <summary>
        /// Gets or sets helyes válasz tartalma.
        /// </summary>
        public char Answer
        {
            get
            {
                return this.answer;
            }

            set
            {
                this.answer = value;
            }
        }

        /// <summary>
        /// Gets or sets kérdés tartalma.
        /// </summary>
        public string Content
        {
            get
            {
                return this.content;
            }

            set
            {
                this.content = value;
            }
        }
    }
}
