﻿//-----------------------------------------------------------------------
// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Adatkötést támagató osztály.
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// Property changed eseménye.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Ez kerül meghívásra az adatkötések folyamán.
        /// </summary>
        /// <param name="s">Adatkötni kívánt elem neve.</param>
        protected void Opc([CallerMemberName] string s = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
    }
}
