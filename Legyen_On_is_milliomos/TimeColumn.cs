﻿//-----------------------------------------------------------------------
// <copyright file="TimeColumn.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;
    using System.Windows;

    /// <summary>
    /// A még hátralévő időt jellemző osztály.
    /// </summary>
    public class TimeColumn : Bindable
    {
        private Rect timeeColumn;
        private Rect backgroudTimeColumn;
        private Rect borderTimeColumn;
        private double decreaseValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeColumn"/> class.
        /// Létrejönnnek a időjeéző elemei.
        /// </summary>
        public TimeColumn()
        {
            this.borderTimeColumn = new Rect(882, 896, 156, 133);
            this.backgroudTimeColumn = new Rect(this.borderTimeColumn.X + 7, this.borderTimeColumn.Y + 7, this.borderTimeColumn.Width - 14, this.borderTimeColumn.Height - 14);
            this.timeeColumn = new Rect(this.borderTimeColumn.X + 7, this.borderTimeColumn.Y + 7, this.borderTimeColumn.Width - 14, this.borderTimeColumn.Height - 14);
            this.decreaseValue = this.timeeColumn.Height / 800;
            this.Opc("timeeColumn");
            this.Opc("backgroudTimeColumn");
        }

        /// <summary>
        /// Gets or sets időt megjelenítő téglalap (kék).
        /// </summary>
        public Rect TimeeColumn
        {
            get
            {
                return this.timeeColumn;
            }

            set
            {
                this.timeeColumn = value;
            }
        }

        /// <summary>
        /// Gets or sets időt megjelenítő téglalap háttere (narancssárga).
        /// </summary>
        public Rect BackgroudTimeColumn
        {
            get
            {
                return this.backgroudTimeColumn;
            }

            set
            {
                this.backgroudTimeColumn = value;
            }
        }

        /// <summary>
        /// Gets or sets időt megjelenítő téglalap kerete (fehér).
        /// </summary>
        public Rect BorderTimeColumn
        {
            get
            {
                return this.borderTimeColumn;
            }

            set
            {
                this.borderTimeColumn = value;
            }
        }

        /// <summary>
        /// Csökkenti az időt.
        /// </summary>
        /// <returns>Visszaadja hogy még nem-e járt le az idő.</returns>
        public bool Decrease()
        {
            bool decrease = false;
            if (this.timeeColumn.Height - this.decreaseValue > 0)
            {
                decrease = true;
                this.timeeColumn.Height -= this.decreaseValue;
                this.timeeColumn.Y += this.decreaseValue;
                this.Opc("timeeColumn");
                Console.WriteLine("magassag; " + this.timeeColumn.Height);
            }

            return decrease;
        }

        /// <summary>
        /// Visszaállítja az időt.
        /// </summary>
        public void Reset()
        {
            this.timeeColumn = new Rect(889, 903, 142, 119);
        }
    }
}
