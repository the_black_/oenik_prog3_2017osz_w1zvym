﻿//-----------------------------------------------------------------------
// <copyright file="AudienceColumns.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Közönség segítség működését leíró osztály
    /// </summary>
    public class AudienceColumns : Bindable
    {
        private static Random r = new Random();
        private int achance;
        private int bchance;
        private int cchance;
        private int dchance;
        private int min;
        private int max;
        private double atempo;
        private double btempo;
        private double ctempo;
        private double dtempo;

        private Rect a;
        private Rect b;
        private Rect c;
        private Rect d;
        private Rect background;

        /// <summary>
        /// Initializes a new instance of the <see cref="AudienceColumns"/> class.
        /// Létrehozza a közönség segítség hátterét és az esély oszlopokat.
        /// </summary>
        public AudienceColumns()
        {
            this.background = new Rect(108, 430, 284, 298);
            this.a = new Rect(128, 660, 38, 0);
            this.b = new Rect(196.6, 660, 38, 0);
            this.c = new Rect(265.2, 660, 38, 0);
            this.d = new Rect(333.8, 660, 38, 0);
        }

        /// <summary>
        /// Gets or sets A esély téglalapja.
        /// </summary>
        public Rect A
        {
            get
            {
                return this.a;
            }

            set
            {
                this.a = value;
            }
        }

        /// <summary>
        /// Gets or sets B esély téglalapja.
        /// </summary>
        public Rect B
        {
            get
            {
                return this.b;
            }

            set
            {
                this.b = value;
            }
        }

        /// <summary>
        /// Gets or sets C esély téglalapja.
        /// </summary>
        public Rect C
        {
            get
            {
                return this.c;
            }

            set
            {
                this.c = value;
            }
        }

        /// <summary>
        /// Gets or sets D esély téglalapja.
        /// </summary>
        public Rect D
        {
            get
            {
                return this.d;
            }

            set
            {
                this.d = value;
            }
        }

        /// <summary>
        /// Gets or sets közönség segítség hetterének téglalapja.
        /// </summary>
        public Rect Background
        {
            get
            {
                return this.background;
            }

            set
            {
                this.background = value;
            }
        }

        /// <summary>
        /// Gets Közönség segítség háttere.
        /// </summary>
        public Brush AudienceBackgroundBrush
        {
            get
            {
                return Processing.GetBrush("picture/audience_help_background.jpg");
            }
        }

        /// <summary>
        /// A könzés segítség oszlopait a megfelelő gyorsasággal megnöveli.
        /// </summary>
        public void Grow()
        {
            if (this.a.Height < this.achance * 2.25)
            {
                this.a.Height += this.atempo;
                this.a.Y -= this.atempo;
                this.b.Height += this.btempo;
                this.b.Y -= this.btempo;
                this.c.Height += this.ctempo;
                this.c.Y -= this.ctempo;
                this.d.Height += this.dtempo;
                this.d.Y -= this.dtempo;
                this.Opc("a");
                this.Opc("b");
                this.Opc("c");
                this.Opc("d");
            }
        }

        /// <summary>
        /// Más-más összegeknél más-más esélyt generál a helyes válaszra
        /// </summary>
        /// <param name="lvl">Az aktuális kérdés sorszámár jelöli</param>
        /// <param name="letter">A kérdés megoldásának helyes betűjele</param>
        public void CalculateChance(int lvl, char letter)
        {
            if (lvl < 5)
            {
                this.min = 70;
                this.max = 80;
            }
            else if (lvl < 10)
            {
                this.min = 40;
                this.max = 60;
            }
            else
            {
                this.min = 30;
                this.max = 40;
            }

            switch (letter)
            {
                case 'A':
                    this.achance = r.Next(this.min, this.max);
                    this.bchance = r.Next(100 - this.achance);
                    this.cchance = r.Next(100 - this.achance - this.bchance);
                    this.dchance = 100 - this.achance - this.bchance - this.cchance;
                    break;
                case 'B':
                    this.bchance = r.Next(this.min, this.max);
                    this.achance = r.Next(100 - this.bchance);
                    this.cchance = r.Next(100 - this.achance - this.bchance);
                    this.dchance = 100 - this.achance - this.bchance - this.cchance;
                    break;
                case 'C':
                    this.cchance = r.Next(this.min, this.max);
                    this.achance = r.Next(100 - this.cchance);
                    this.bchance = r.Next(100 - this.achance - this.cchance);
                    this.dchance = 100 - this.achance - this.bchance - this.cchance;
                    break;
                case 'D':
                    this.dchance = r.Next(this.min, this.max);
                    this.achance = r.Next(100 - this.dchance);
                    this.bchance = r.Next(100 - this.achance - this.dchance);
                    this.cchance = 100 - this.achance - this.bchance - this.dchance;
                    break;
            }

            this.atempo = (double)this.achance / 15;
            this.btempo = (double)this.bchance / 15;
            this.ctempo = (double)this.cchance / 15;
            this.dtempo = (double)this.dchance / 15;
        }
    }
}