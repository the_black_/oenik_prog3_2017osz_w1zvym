﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel vm;
        private DispatcherTimer dtTimeColumn;
        private DispatcherTimer dtNewQuestion;
        private DispatcherTimer dtStar;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// A játék megnyitása utáni közvetlen dolgokat végzi kell.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            Processing.SetNewFilePath();
            this.vm = new ViewModel(this);
            this.DataContext = this.vm;
            this.d_group.Children.Clear();
            this.dtTimeColumn = new DispatcherTimer();
            this.dtTimeColumn.Interval = TimeSpan.FromMilliseconds(20);
            this.dtTimeColumn.Tick += this.DTTimeColumnTick;
            this.dtTimeColumn.Start();

            this.dtNewQuestion = new DispatcherTimer();
            this.dtNewQuestion.Interval = TimeSpan.FromMilliseconds(30);
            this.dtNewQuestion.Tick += this.DtNewQuestion;
            this.dtNewQuestion.Start();

            this.dtStar = new DispatcherTimer();
            this.dtStar.Interval = TimeSpan.FromMilliseconds(10);
            this.dtStar.Tick += this.DTStarTick;

            if (this.vm.MenuuLogic.Marker == 0)
            {
                this.vm.BG.MenuScreenElements(); // lérejönnek a Rect-ek stb.
                this.vm.Screen.PutMenuScreenElements(); // a lértrejött Rect-eket megjelenítem
                this.vm.DispMenuString();
            }
        }

        /// <summary>
        /// Gets or sets az idő oszlopát időzitője
        /// </summary>
        public DispatcherTimer DTTimeColumn
        {
            get
            {
                return this.dtTimeColumn;
            }

            set
            {
                this.dtTimeColumn = value;
            }
        }

        /// <summary>
        /// Gets or sets a jutalom csillag időzítője
        /// </summary>
        public DispatcherTimer DTStar
        {
            get
            {
                return this.dtStar;
            }

            set
            {
                this.dtStar = value;
            }
        }

        /// <summary>
        /// A játék végén a jutalom megjelenítését indítja el.
        /// </summary>
        /// <param name="sender">Küldő paramétere.</param>
        /// <param name="e">Esemény paramétere.</param>
        private void DTStarTick(object sender, EventArgs e)
        {
            if (this.vm.MenuuLogic.Marker == -1)
            {
                this.vm.Star.Animate(15);
                this.vm.Star.PrintText(ViewModel.Player.Money.ToString() + " Ft");
            }
        }

        /// <summary>
        /// Új kérdést írja ki.
        /// </summary>
        /// <param name="sender">Küldő paramétere.</param>
        /// <param name="e">Esemény paramétere.</param>
        private void DtNewQuestion(object sender, EventArgs e)
        {
            if (this.vm.MenuuLogic.Marker == 1)
            {
                this.vm.GameeLogic.NextQuestion(this.vm, this);
            }
        }

        /// <summary>
        /// A hátralévő időt vezérli.
        /// </summary>
        /// <param name="sender"> Küldő paramétere.</param>
        /// <param name="e">Esemény paramétere.</param>
        private void DTTimeColumnTick(object sender, EventArgs e)
        {
            if (this.vm.MenuuLogic.Marker == 1)
            {
                ViewModel.Player.Gameover = !this.vm.AnimatedTimeColumn.Decrease();
                if (this.vm.GameeLogic.Audience == true)
                {
                    this.vm.AudienceHelp.Grow();
                }
            }
        }

        /// <summary>
        /// Bal klikk le eseménykezelője. A megjelölést, kiválasztást kezeli le.
        /// </summary>
        /// <param name="sender">Küldő paramétere.</param>
        /// <param name="e">Kattintás eseénye.</param>
        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.vm.ClickkManager.ClickOnGame(e, this.vm, this);
        }

        /// <summary>
        /// Bal klikk fel eseménykezelője. Ez ellenőrzi a válasz helyességét.
        /// </summary>
        /// <param name="sender">Küldő paramétere.</param>
        /// <param name="e">Esemény paramétere.</param>
        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.vm.MenuuLogic.Marker == 1)
            {
                this.vm.GameeLogic.CheckAnswer(this.vm);
            }
        }

        /// <summary>
        /// Csalás a játkbam, megmutatja a jó választ.
        /// </summary>
        /// <param name="sender">Küldő paramétere.</param>
        /// <param name="e">Esemény paramétere.</param>
        private void Image_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (this.vm.MenuuLogic.Marker == 1)
            {
                this.vm.GameeLogic.Cheat(this.vm);
            }
        }

        /// <summary>
        /// Játék indulásakori zenét tölti be rögötön.
        /// </summary>
        /// <param name="sender">Küldő paramétere.</param>
        /// <param name="e">Esemény paramétere.</param>
        private void Image_Loaded(object sender, RoutedEventArgs e)
        {
            Processing.PlaySong("music.mp3");
        }
    }
}
