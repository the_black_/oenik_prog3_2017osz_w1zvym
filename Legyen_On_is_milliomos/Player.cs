﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System;

    /// <summary>
    /// A játékmódotak tartalmazó enum.
    /// </summary>
    public enum GameMode
    {
        /// <summary>
        /// Segítséges játékmód.
        /// </summary>
        Withhelp,

        /// <summary>
        /// Biztos pénz játékmód.
        /// </summary>
        Suremoney,

        /// <summary>
        /// Duplatipp játékmód.
        /// </summary>
        Doubletipp
    }

    /// <summary>
    /// Játékost reprezentáló osztály.
    /// </summary>
    public class Player : IComparable
    {
        private string name;
        private int money;
        private int level;
        private bool gameover;
        private bool questionwon;
        private char playeranswerletter; // játékos ezt a választ jelöli meg (A,B,C,D)
        private char playerAnswer2Letter;
        private GameMode gameM;
        private int guaranteedMoney;
        private bool stopGame;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// A játékos konstruktora.
        /// </summary>
        /// <param name="name">A játékos neve</param>
        /// <param name="money">A játékos nyereménye</param>
        public Player(string name, int money)
        {
            this.money = money;
            this.name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// A játékos paraméternélküli konstruktorja.
        /// </summary>
        public Player()
        {
            this.money = 0;
            this.level = 1;
            this.gameover = false;
            this.questionwon = false;
            this.guaranteedMoney = 0;
            this.stopGame = false;
        }

        /// <summary>
        /// Gets or sets a kérdés és egyben a nyereményt jelölő sorszám.
        /// </summary>
        public int Level
        {
            get
            {
                return this.level;
            }

            set
            {
                this.level = value;
            }
        }

        /// <summary>
        /// Gets játékmódokat tömbben.
        /// </summary>
        public Array GameModes
        {
            get
            {
                return Enum.GetValues(typeof(GameMode));
            }
        }

        /// <summary>
        /// Gets or sets játékos pénze.
        /// </summary>
        public int Money
        {
            get
            {
                return this.money;
            }

            set
            {
                this.money = value;
            }
        }

        /// <summary>
        /// Gets or sets játékos neve.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether vége e a játéknak.
        /// </summary>
        public bool Gameover
        {
            get
            {
                return this.gameover;
            }

            set
            {
                this.gameover = value;
            }
        }

        /// <summary>
        /// Gets or sets játékos megjelölt válaszának betűjele.
        /// </summary>
        public char Playeranswerletter
        {
            get
            {
                return this.playeranswerletter;
            }

            set
            {
                this.playeranswerletter = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a kérdést jól válaszolta e meg.
        /// </summary>
        public bool Questionwon
        {
            get
            {
                return this.questionwon;
            }

            set
            {
                this.questionwon = value;
            }
        }

        /// <summary>
        /// Gets or sets játékmód.
        /// </summary>
        public GameMode GameM
        {
            get
            {
                return this.gameM;
            }

            set
            {
                this.gameM = value;
            }
        }

        /// <summary>
        /// Gets or sets a biztos nyeremény összege.
        /// </summary>
        public int GuaranteedMoney
        {
            get
            {
                return this.guaranteedMoney;
            }

            set
            {
                this.guaranteedMoney = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether megállt-e.
        /// </summary>
        public bool StopGame
        {
            get
            {
                return this.stopGame;
            }

            set
            {
                this.stopGame = value;
            }
        }

        /// <summary>
        /// Gets or sets duplatipp esetén a második tipp betűjele.
        /// </summary>
        public char PlayerAnswer2Letter
        {
            get
            {
                return this.playerAnswer2Letter;
            }

            set
            {
                this.playerAnswer2Letter = value;
            }
        }

        /// <summary>
        /// Felülírja a != operátort.
        /// </summary>
        /// <param name="p1">összehasonlító játékos</param>
        /// <param name="p2">össehasonlítandó játékos</param>
        /// <returns>összehasonlítás eredménye</returns>
        public static bool operator !=(Player p1, Player p2)
        {
            if (p1 is Player && p2 is Player)
            {
                return p1.money != p2.money;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Felülírja a == operátort.
        /// </summary>
        /// <param name="p1">összehasonlító játékos</param>
        /// <param name="p2">össehasonlítandó játékos</param>
        /// <returns>összehasonlítás eredménye</returns>
        public static bool operator ==(Player p1, Player p2)
        {
            if (p1 is Player && p2 is Player)
            {
                return p1.money == p2.money;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Felülírja a nagyobb operátort.
        /// </summary>
        /// <param name="p1">összehasonlító játékos</param>
        /// <param name="p2">össehasonlítandó játékos</param>
        /// <returns>összehasonlítás eredménye</returns>
        public static bool operator >(Player p1, Player p2)
        {
            return p1.money > p2.money;
        }

        /// <summary>
        /// Felülírja kisebb operátort.
        /// </summary>
        /// <param name="p1">összehasonlító játékos</param>
        /// <param name="p2">össehasonlítandó játékos</param>
        /// <returns>összehasonlítás eredménye</returns>
        public static bool operator <(Player p1, Player p2)
        {
            return p1.money < p2.money;
        }

        /// <summary>
        /// Rendezésné összehasonlítja a a játékosokat.
        /// </summary>
        /// <param name="obj">Az éppen vizsgálandó játékost tartalmazza.</param>
        /// <returns>Összehsonlítás eredménye.</returns>
        public int CompareTo(object obj)
        {
            Player second = obj as Player;
            if (this.money > second.money)
            {
                return -1;
            }
            else if (this.money < second.money)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Megvizsgálja hogy a két játékosnak azonos a nyereménye.
        /// </summary>
        /// <param name="obj">vizsgálandó játékos</param>
        /// <returns>összehasonlítás eredménye</returns>
        public override bool Equals(object obj)
        {
            return this.money == (obj as Player).Money;
        }

        /// <summary>
        /// Felülírja a gethascode metódust.
        /// </summary>
        /// <returns>új gethashcode érték</returns>
        public override int GetHashCode()
        {
            return this.money.GetHashCode();
        }

        /// <summary>
        /// A fájlba formázottan íráshoz szükséges.
        /// </summary>
        /// <returns>Formázoott sor a ranglista txt-nek</returns>
        public override string ToString()
        {
            return "\t\t" + this.name + "\t\t\t" + this.money;
        }
    }
}
