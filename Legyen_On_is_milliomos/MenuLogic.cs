﻿//-----------------------------------------------------------------------
// <copyright file="MenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    /// <summary>
    /// A marker változó jelöli, hogy aktuálisan melyik részén vagyunk a kátéknak (vége, menü, játék)
    /// </summary>
    public class MenuLogic
    {
        private int marker;

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuLogic"/> class.
        /// Nullára állítja az aktuális menüpontot jelölő számot.
        /// </summary>
        public MenuLogic()
        {
            this.marker = 0;
        }

        /// <summary>
        /// Gets or sets menüt jelölő szám.
        /// </summary>
        public int Marker
        {
            get
            {
                return this.marker;
            }

            set
            {
                this.marker = value;
            }
        }
    }
}
