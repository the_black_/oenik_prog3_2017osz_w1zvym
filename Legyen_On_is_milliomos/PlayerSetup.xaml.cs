﻿//-----------------------------------------------------------------------
// <copyright file="PlayerSetup.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;

    /// <summary>
    /// Játékos adatainak bekérését támogatja.
    /// </summary>
    public partial class PlayerSetup : Window
    {
        private int characterCounter;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerSetup"/> class.
        /// Létrehozz egy új játékost.
        /// </summary>
        public PlayerSetup()
        {
            this.InitializeComponent();
            this.NewPlayer = new Player();
            this.DataContext = this.NewPlayer;
            this.characterCounter = 0;
        }

        /// <summary>
        /// Gets or sets új játékos.
        /// </summary>
        public Player NewPlayer { get; set; }

        /// <summary>
        /// Az adatkötést valósítja meg.
        /// </summary>
        /// <param name="sender">Küldő objektum-</param>
        /// <param name="e">RoutedEventArgs</param>
        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            BindingExpression be = this.tb_name.GetBindingExpression(TextBox.TextProperty);
            be.UpdateSource();

            be = this.cbox_mode.GetBindingExpression(ComboBox.SelectedItemProperty);
            be.UpdateSource();
            this.DialogResult = true;
            this.Close();
        }
    }
}