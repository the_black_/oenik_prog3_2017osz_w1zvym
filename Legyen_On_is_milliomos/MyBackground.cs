﻿//-----------------------------------------------------------------------
// <copyright file="MyBackground.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Közel az összes "háttérrétegeket" tartalmazza (kivéve az időjelző, és a csillag) amelyek a játék folyamán előjöhetnek. A XAML fájl is ide van kötve fpleg.
    /// </summary>
    public class MyBackground : Bindable
    {
        private static Color blue = Color.FromArgb(255, 24, 25, 108);
        private static Color orange = Color.FromArgb(255, 240, 146, 32);

        private Rect gameBackground;           // játék háttere

        private Rect questionBorder;     // kérdés fehér háttere
        private Rect questionBackground;     // kérdés kék hátter

        private Rect answerABorder;     // A válasz fehér háttere
        private Rect answerABackground;     // A válasz kék háttere
        private Rect answerBBorder;     // B válasz fehér háttere
        private Rect answerBBackground;     // B válasz kék háttere
        private Rect answerCBorder;     // C válasz fehér háttere
        private Rect answerCBackground;     // C válasz kék háttere
        private Rect answerDBorder;     // D válasz fehér háttere
        private Rect answerDBackground;     // D válasz kék háttere

        private Rect bar; // nyeremény jelölő
        private Rect moneyBorder;

        private Rect audience;
        private Rect doubletipp;
        private Rect exit;
        private Rect ft;
        private Rect half;
        private Rect phone;
        private Rect phoneTippBackground;

        private Rect menu;
        private Rect newGame;
        private Rect rules;
        private Rect top;
        private Rect exitGame;

        private Brush aBackgroundBrush;
        private Brush bBackgroundBrush;
        private Brush cBackgroundBrush;
        private Brush dBackgroundBrush;

        private Rect starRect;

        private Rect rulesTopPlayers;
        private Pen borderPen;

        /// <summary>
        /// Initializes a new instance of the <see cref="MyBackground"/> class.
        /// Beállítja a játék kék színét és a fehér keret pen-jét.
        /// </summary>
        public MyBackground()
        {
            this.borderPen = new Pen(new SolidColorBrush(Colors.White), 7); // rögtön kell
            blue = Color.FromArgb(255, 24, 25, 108); // rögtön kell
        }

        /// <summary>
        /// Gets kék szín.
        /// </summary>
        public static Brush BlueBrush
        {
            get { return new SolidColorBrush(blue); }
        }

        /// <summary>
        /// Gets narancssárga szín.
        /// </summary>
        public static Brush OrangeBrush
        {
            get { return new SolidColorBrush(orange); }
        }

        /// <summary>
        /// Gets or sets kék szín.
        /// </summary>
        public static Color Blue
        {
            get
            {
                return blue;
            }

            set
            {
                blue = value;
            }
        }

        /// <summary>
        /// Gets or sets narancssárga szín.
        /// </summary>
        public static Color Orange
        {
            get
            {
                return orange;
            }

            set
            {
                orange = value;
            }
        }

        /// <summary>
        /// Gets or sets jutalom csillag téglalapja.
        /// </summary>
        public Rect StarRect
        {
            get
            {
                return this.starRect;
            }

            set
            {
                this.starRect = value;
            }
        }

        /// <summary>
        /// Gets jutalom csillag képe.
        /// </summary>
        public Brush StarBrush
        {
            get
            {
                return Processing.GetBrush("picture/star.png");
            }
        }

        /// <summary>
        /// Gets or sets maga a játék háttere.
        /// </summary>
        public Rect GameBackground
        {
            get
            {
                return this.gameBackground;
            }

            set
            {
                this.gameBackground = value;
            }
        }

        /// <summary>
        /// Gets or sets kérdés fehér háttere.
        /// </summary>
        public Rect QuestionBorder
        {
            get
            {
                return this.questionBorder;
            }

            set
            {
                this.questionBorder = value;
            }
        }

        /// <summary>
        /// Gets or sets kérdés két háttere.
        /// </summary>
        public Rect QuestionBackground
        {
            get
            {
                return this.questionBackground;
            }

            set
            {
                this.questionBackground = value;
            }
        }

        /// <summary>
        /// Gets or sets A válasz fehér háttere.
        /// </summary>
        public Rect AnswerABorder
        {
            get
            {
                return this.answerABorder;
            }

            set
            {
                this.answerABorder = value;
            }
        }

        /// <summary>
        /// Gets or sets A válasz kék háttere.
        /// </summary>
        public Rect AnswerABackground
        {
            get
            {
                return this.answerABackground;
            }

            set
            {
                this.answerABackground = value;
            }
        }

        /// <summary>
        /// Gets or sets B válasz fehér háttere.
        /// </summary>
        public Rect AnswerBBorder
        {
            get
            {
                return this.answerBBorder;
            }

            set
            {
                this.answerBBorder = value;
            }
        }

        /// <summary>
        /// Gets or sets B válasz kék háttere.
        /// </summary>
        public Rect AnswerBBackground
        {
            get
            {
                return this.answerBBackground;
            }

            set
            {
                this.answerBBackground = value;
            }
        }

        /// <summary>
        /// Gets or sets C válasz fehér háttere.
        /// </summary>
        public Rect AnswerCBorder
        {
            get
            {
                return this.answerCBorder;
            }

            set
            {
                this.answerCBorder = value;
            }
        }

        /// <summary>
        /// Gets or sets C válasz kék háttere.
        /// </summary>
        public Rect AnswerCBackground
        {
            get
            {
                return this.answerCBackground;
            }

            set
            {
                this.answerCBackground = value;
            }
        }

        /// <summary>
        /// Gets or sets D válasz fehér háttere.
        /// </summary>
        public Rect AnswerDBorder
        {
            get
            {
                return this.answerDBorder;
            }

            set
            {
                this.answerDBorder = value;
            }
        }

        /// <summary>
        /// Gets or sets D válasz kék háttere.
        /// </summary>
        public Rect AnswerDBackground
        {
            get
            {
                return this.answerDBackground;
            }

            set
            {
                this.answerDBackground = value;
            }
        }

        /// <summary>
        /// Gets or sets nyereény jelölő téglalapja.
        /// </summary>
        public Rect Bar
        {
            get
            {
                return this.bar;
            }

            set
            {
                this.bar = value;
            }
        }

        /// <summary>
        /// Gets or sets Nyeremények háttere.
        /// </summary>
        public Rect MoneyBorder
        {
            get
            {
                return this.moneyBorder;
            }

            set
            {
                this.moneyBorder = value;
            }
        }

        /// <summary>
        /// Gets or sets közönség segítség téglapja.
        /// </summary>
        public Rect Audience
        {
            get
            {
                return this.audience;
            }

            set
            {
                this.audience = value;
            }
        }

        /// <summary>
        /// Gets or sets duiplatipp téglalapja.
        /// </summary>
        public Rect Doubletipp
        {
            get
            {
                return this.doubletipp;
            }

            set
            {
                this.doubletipp = value;
            }
        }

        /// <summary>
        /// Gets or sets kilépés téglalapja.
        /// </summary>
        public Rect Exit
        {
            get
            {
                return this.exit;
            }

            set
            {
                this.exit = value;
            }
        }

        /// <summary>
        /// Gets or sets megállás téglalpja.
        /// </summary>
        public Rect FT
        {
            get
            {
                return this.ft;
            }

            set
            {
                this.ft = value;
            }
        }

        /// <summary>
        /// Gets or sets felezés téglalpja.
        /// </summary>
        public Rect Half
        {
            get
            {
                return this.half;
            }

            set
            {
                this.half = value;
            }
        }

        /// <summary>
        /// Gets or sets telefon téglalapja.
        /// </summary>
        public Rect Phone
        {
            get
            {
                return this.phone;
            }

            set
            {
                this.phone = value;
            }
        }

        /// <summary>
        /// Gets or sets telefonos segítség válaszának téglalpja.
        /// </summary>
        public Rect PhoneTippBackground
        {
            get
            {
                return this.phoneTippBackground;
            }

            set
            {
                this.phoneTippBackground = value;
            }
        }

        /// <summary>
        /// Gets or sets menü háttere.
        /// </summary>
        public Rect Menu
        {
            get
            {
                return this.menu;
            }

            set
            {
                this.menu = value;
            }
        }

        /// <summary>
        /// Gets or sets új játék téglalapja a menüben.
        /// </summary>
        public Rect NewGame
        {
            get
            {
                return this.newGame;
            }

            set
            {
                this.newGame = value;
            }
        }

        /// <summary>
        /// Gets or sets szabályok téglalapja a menüben.
        /// </summary>
        public Rect Rules
        {
            get
            {
                return this.rules;
            }

            set
            {
                this.rules = value;
            }
        }

        /// <summary>
        /// Gets or sets ranglista téglalapja a menüben.
        /// </summary>
        public Rect Top
        {
            get
            {
                return this.top;
            }

            set
            {
                this.top = value;
            }
        }

        /// <summary>
        /// Gets or sets kilépés téglalapja a menüben.
        /// </summary>
        public Rect ExitGame
        {
            get
            {
                return this.exitGame;
            }

            set
            {
                this.exitGame = value;
            }
        }

        /// <summary>
        /// Gets or sets a szabályok és a toplistás játékosok háttere a menüben.
        /// </summary>
        public Rect RulesTopPlayersRect
        {
            get
            {
                return this.rulesTopPlayers;
            }

            set
            {
                this.rulesTopPlayers = value;
            }
        }

        /// <summary>
        /// Gets or sets A válasz színe.
        /// </summary>
        public Brush ABackgroundBrush
        {
            get
            {
                return this.aBackgroundBrush;
            }

            set
            {
                this.aBackgroundBrush = value;
                this.Opc("aBackgroundBrush");
            }
        }

        /// <summary>
        /// Gets or sets B válasz színe.
        /// </summary>
        public Brush BBackgroundBrush
        {
            get
            {
                return this.bBackgroundBrush;
            }

            set
            {
                this.bBackgroundBrush = value;
                this.Opc("bBackgroundBrush");
            }
        }

        /// <summary>
        /// Gets or sets C válasz színe.
        /// </summary>
        public Brush CBackgroundBrush
        {
            get
            {
                return this.cBackgroundBrush;
            }

            set
            {
                this.cBackgroundBrush = value;
                this.Opc("cBackgroundBrush");
            }
        }

        /// <summary>
        /// Gets or sets D válasz színe.
        /// </summary>
        public Brush DBackgroundBrush
        {
            get
            {
                return this.dBackgroundBrush;
            }

            set
            {
                this.dBackgroundBrush = value;
                this.Opc("dBackgroundBrush");
            }
        }

        /// <summary>
        /// Gets a játék háttérképe.
        /// </summary>
        public Brush BackgroundBrush
        {
            get
            {
                return Processing.GetBrush("picture/background.jpg");
            }
        }

        /// <summary>
        /// Gets menü háttérképe.
        /// </summary>
        public Brush MenuBackgroundBrush
        {
            get
            {
                return Processing.GetBrush("picture/menu_background.jpg");
            }
        }

        /// <summary>
        /// Gets nyeremény összeget jelölő színe.
        /// </summary>
        public Brush MoneyBarBrush
        {
            get { return new SolidColorBrush(Color.FromArgb(130, 144, 90, 236)); }
        }

        /// <summary>
        /// Gets nyereények kerete.
        /// </summary>
        public Brush MoneyBorderBrush
        {
            get
            {
                return Processing.GetBrush("picture/money_border.png");
            }
        }

        /// <summary>
        /// Gets közönség segítség ikonja.
        /// </summary>
        public Brush AudienceBrush
        {
            get
            {
                return Processing.GetBrush("picture/audience.jpg");
            }
        }

        /// <summary>
        /// Gets duplatipp ikonja.
        /// </summary>
        public Brush DoubletippBrush
        {
            get
            {
                return Processing.GetBrush("picture/doubletipp.jpg");
            }
        }

        /// <summary>
        /// Gets kilépés ikonja.
        /// </summary>
        public Brush ExitBrush
        {
            get
            {
                return Processing.GetBrush("picture/exit.jpg");
            }
        }

        /// <summary>
        /// Gets megállás ikonja.
        /// </summary>
        public Brush FTBrush
        {
            get
            {
                return Processing.GetBrush("picture/ft.jpg");
            }
        }

        /// <summary>
        /// Gets felezés ikonja.
        /// </summary>
        public Brush HalfBrush
        {
            get
            {
                return Processing.GetBrush("picture/half.jpg");
            }
        }

        /// <summary>
        /// Gets telefonos segítség ikonja.
        /// </summary>
        public Brush PhoneBrush
        {
            get
            {
                return Processing.GetBrush("picture/phone.jpg");
            }
        }

        /// <summary>
        /// Gets or sets fehér keret pen.
        /// </summary>
        public Pen BorderPen
        {
            get
            {
                return this.borderPen;
            }

            set
            {
                this.borderPen = value;
            }
        }

        /// <summary>
        /// A menünek az elemeit ozza létre.
        /// </summary>
        public void MenuScreenElements()
        {
            this.menu = new Rect(0, 0, 1920, 1080);
            this.newGame = new Rect(715, 548, 490, 100);
            this.rules = new Rect(715, 663, 490, 100);
            this.top = new Rect(715, 778, 490, 100);
            this.exitGame = new Rect(715, 893, 490, 100);
        }

        /// <summary>
        /// Magát a játéknak a képernyőjét hozza létre (itt kell a kérdésekre válaszolni).
        /// </summary>
        public void GameScreenElements()
        {
            this.gameBackground = new Rect(0, 0, 1920, 1080);
            this.questionBorder = new Rect(108, 743, 1704, 139);
            this.questionBackground = new Rect(108 + 7, 743 + 7, 1704 - 14, 139 - 14);

            // A téglalapja
            this.answerABorder = new Rect(108, 896, 767, 63);
            this.answerABackground = new Rect(108 + 7, 896 + 7, 767 - 14, 63 - 14);

            // B téglalapja
            this.answerBBorder = new Rect(1045, 896, 767, 63);
            this.answerBBackground = new Rect(1045 + 7, 896 + 7, 767 - 14, 63 - 14);

            // C téglalapja
            this.answerCBorder = new Rect(108, 966, 767, 63);
            this.answerCBackground = new Rect(108 + 7, 966 + 7, 767 - 14, 63 - 14);

            // D téglalapja
            this.answerDBorder = new Rect(1045, 966, 767, 63);
            this.answerDBackground = new Rect(1045 + 7, 966 + 7, 767 - 14, 63 - 14);

            this.bar = new Rect(1486, 680, 319, 36); // aktuális nyeremény jelölő

            this.moneyBorder = new Rect(1479, 165, 333, 564); // nyeremény kerete

            this.exit = new Rect(73, 42, 126, 116);
            this.ft = new Rect(206, 42, 126, 116);
            this.audience = new Rect(1455, 42, 126, 116);
            this.half = new Rect(1588, 42, 126, 116);
            this.phone = new Rect(1721, 42, 126, 116);

            this.doubletipp = new Rect(1587, 42, 126, 116);

            this.phoneTippBackground = new Rect(760, 665, 400, 63);
        }

        /// <summary>
        /// Létrehozza a szabályok és a top játékosok téglalpját.
        /// </summary>
        public void RulesTopPlayersRectCreate()
        {
            this.rulesTopPlayers = new Rect(1220, 548, 650, 445);
        }

        /// <summary>
        /// Visszaállítja az összes válaszlehetőség betűjelét.
        /// </summary>
        public void ResetAnswerBackgroundBrush()
        {
            this.ABackgroundBrush = BlueBrush;
            this.BBackgroundBrush = BlueBrush;
            this.CBackgroundBrush = BlueBrush;
            this.DBackgroundBrush = BlueBrush;
        }

        /// <summary>
        /// Beszínezi a parméterül kapott válaszlehetőséget a megadott színre (jelölés, helyes válasz, rossz válasz)
        /// </summary>
        /// <param name="letter">A megváltoztantó válaszlehetőség betűjele</param>
        /// <param name="color">A válaszlehetőség színe, amire be szeretnénk állítani</param>
        public void ColorAnswer(char letter, Color color)
        {
            switch (letter)
            {
                case 'A': this.ABackgroundBrush = new SolidColorBrush(color); break;
                case 'B': this.BBackgroundBrush = new SolidColorBrush(color); break;
                case 'C': this.CBackgroundBrush = new SolidColorBrush(color); break;
                case 'D': this.DBackgroundBrush = new SolidColorBrush(color); break;
            }
        }

        /// <summary>
        /// Mozgatja a nyereményjelölőt.
        /// </summary>
        public void MooveMoneyBar()
        {
            this.bar.Y -= this.bar.Height;
            this.Opc("bar");
        }
    }
}
