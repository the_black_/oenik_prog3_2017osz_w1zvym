﻿//-----------------------------------------------------------------------
// <copyright file="ClickManager.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Lekezeli a az egér bal gombjának lenyomását
    /// </summary>
    public class ClickManager
    {
        /// <summary>
        /// Lekezeli a bal egérgomb lenyomását
        /// </summary>
        /// <param name="e">egér eseménye</param>
        /// <param name="vm">hivatkozás a ViewModelre</param>
        /// <param name="main">hivatkozás a main windowsra</param>
        public void ClickOnGame(MouseEventArgs e, ViewModel vm, MainWindow main) // a játék főképernyőjén lévő kattintásokat kezeli le
        {
            Point mousePosition = e.GetPosition(main.image);
            if (vm.MenuuLogic.Marker == -1)
            {
                main.Close();
            }

            // eggyáltalán a válaszlehetőségre kattintottunk e és nem másra
            if (mousePosition.X >= 108 && mousePosition.X <= 1812 && mousePosition.Y >= 896 && mousePosition.Y <= 1029 && vm.MenuuLogic.Marker == 1)
            {
                // A
                if (mousePosition.X <= 875 && mousePosition.Y <= 959 && main.d_group.Children.Contains(main.ab))
                {
                    Processing.PlaySong("select_answer.mp3");
                    vm.BG.ABackgroundBrush = new SolidColorBrush(Colors.Orange);
                    vm.GameeLogic.PlayerClickedOnAnser = true;
                    if (vm.GameeLogic.Doubletipp == true)
                    {
                        if (vm.GameeLogic.DoubletippCount == 2)
                        {
                            ViewModel.Player.Playeranswerletter = 'A';
                        }

                        if (vm.GameeLogic.DoubletippCount == 1)
                        {
                            ViewModel.Player.PlayerAnswer2Letter = 'A';
                        }

                        vm.GameeLogic.DoubletippCount--;
                        if (vm.GameeLogic.DoubletippCount == 0)
                        {
                            vm.GameeLogic.Doubletipp = false;
                            main.DTTimeColumn.Stop();
                        }
                    }
                    else
                    {
                        ViewModel.Player.Playeranswerletter = 'A';
                        main.DTTimeColumn.Stop();
                    }
                }

                // C
                else if (mousePosition.X <= 875 && mousePosition.Y >= 966 && mousePosition.Y <= 1029 && main.d_group.Children.Contains(main.cb))
                {
                    Processing.PlaySong("select_answer.mp3");
                    vm.BG.CBackgroundBrush = new SolidColorBrush(Colors.Orange);
                    vm.GameeLogic.PlayerClickedOnAnser = true;
                    if (vm.GameeLogic.Doubletipp == true)
                    {
                        if (vm.GameeLogic.DoubletippCount == 2)
                        {
                            ViewModel.Player.Playeranswerletter = 'C';
                        }

                        if (vm.GameeLogic.DoubletippCount == 1)
                        {
                            ViewModel.Player.PlayerAnswer2Letter = 'C';
                        }

                        vm.GameeLogic.DoubletippCount--;
                        if (vm.GameeLogic.DoubletippCount == 0)
                        {
                            vm.GameeLogic.Doubletipp = false;
                            main.DTTimeColumn.Stop();
                        }
                    }
                    else
                    {
                        ViewModel.Player.Playeranswerletter = 'C';
                        main.DTTimeColumn.Stop();
                    }
                }

                // B
                else if (mousePosition.X >= 1045 && mousePosition.Y <= 959 && main.d_group.Children.Contains(main.bb))
                {
                    Processing.PlaySong("select_answer.mp3");
                    vm.BG.BBackgroundBrush = new SolidColorBrush(Colors.Orange);
                    vm.GameeLogic.PlayerClickedOnAnser = true;
                    if (vm.GameeLogic.Doubletipp == true)
                    {
                        if (vm.GameeLogic.DoubletippCount == 2)
                        {
                            ViewModel.Player.Playeranswerletter = 'B';
                        }

                        if (vm.GameeLogic.DoubletippCount == 1)
                        {
                            ViewModel.Player.PlayerAnswer2Letter = 'B';
                        }

                        vm.GameeLogic.DoubletippCount--;
                        if (vm.GameeLogic.DoubletippCount == 0)
                        {
                            vm.GameeLogic.Doubletipp = false;
                            main.DTTimeColumn.Stop();
                        }
                    }
                    else
                    {
                        ViewModel.Player.Playeranswerletter = 'B';
                        main.DTTimeColumn.Stop();
                    }
                }

                // D
                else if (mousePosition.X >= 1045 && mousePosition.Y >= 966 && mousePosition.Y <= 1029 && main.d_group.Children.Contains(main.db))
                {
                    Processing.PlaySong("select_answer.mp3");
                    vm.BG.DBackgroundBrush = new SolidColorBrush(Colors.Orange);
                    vm.GameeLogic.PlayerClickedOnAnser = true;
                    if (vm.GameeLogic.Doubletipp == true)
                    {
                        if (vm.GameeLogic.DoubletippCount == 2)
                        {
                            ViewModel.Player.Playeranswerletter = 'D';
                        }

                        if (vm.GameeLogic.DoubletippCount == 1)
                        {
                            ViewModel.Player.PlayerAnswer2Letter = 'D';
                        }

                        vm.GameeLogic.DoubletippCount--;
                        if (vm.GameeLogic.DoubletippCount == 0)
                        {
                            vm.GameeLogic.Doubletipp = false;
                            main.DTTimeColumn.Stop();
                        }
                    }
                    else
                    {
                        ViewModel.Player.Playeranswerletter = 'D';
                        main.DTTimeColumn.Stop();
                    }
                }
            }
            else if (mousePosition.X >= 73 && mousePosition.X <= 1847 && mousePosition.Y >= 42 && mousePosition.Y <= 158 && vm.MenuuLogic.Marker == 1)
            {
                // KILÉPÉS
                if (mousePosition.X <= 199)
                {
                    main.DTTimeColumn.Stop();
                    main.Close();
                }

                // MEGÁLL
                else if (mousePosition.X >= 206 && mousePosition.X <= 332)
                {
                    main.DTTimeColumn.Stop();
                    ViewModel.Player.StopGame = true;
                    ViewModel.Player.Gameover = true;
                }

                // KÖZÖNSÉG
                else if (mousePosition.X >= 1455 && mousePosition.X <= 1581 && ViewModel.Player.GameM == GameMode.Withhelp && vm.GameeLogic.Audience == null && !vm.GameeLogic.PlayerClickedOnHelp)
                {
                    vm.GameeLogic.PlayerClickedOnHelp = true;
                    vm.GameeLogic.Audience = true;
                    main.d_group.Children.Remove(main.audience);
                    vm.AudienceHelp.CalculateChance(ViewModel.Player.Level, vm.ActualQuestion.Answer);
                    main.d_group.Children.Add(main.audienceBackground);
                    main.d_group.Children.Add(main.audienceA);
                    main.d_group.Children.Add(main.audienceB);
                    main.d_group.Children.Add(main.audienceC);
                    main.d_group.Children.Add(main.audienceD);
                }

                // FELEZÉS
                else if (mousePosition.X >= 1588 && mousePosition.X <= 1684 && ViewModel.Player.GameM == GameMode.Withhelp && vm.GameeLogic.Half == null && !vm.GameeLogic.PlayerClickedOnHelp)
                {
                    vm.GameeLogic.PlayerClickedOnHelp = true;
                    vm.GameeLogic.Half = true;
                    vm.GameeLogic.HelpHalf(vm);
                    main.d_group.Children.Remove(main.half);
                }

                // TELEFON
                else if (mousePosition.X >= 1721 && mousePosition.X <= 1847 && ViewModel.Player.GameM == GameMode.Withhelp && vm.GameeLogic.Phone == null && !vm.GameeLogic.PlayerClickedOnHelp)
                {
                    vm.GameeLogic.PlayerClickedOnHelp = true;
                    vm.GameeLogic.Phone = true;
                    main.d_group.Children.Remove(main.phone);
                    main.d_group.Children.Add(main.phoneB);
                    vm.GameeLogic.HelpPhone(vm);
                }

                // DUPLATIPP
                else if (mousePosition.X >= 1587 && mousePosition.X <= 1713 && ViewModel.Player.GameM == GameMode.Doubletipp && vm.GameeLogic.Doubletipp == null)
                {
                    vm.GameeLogic.Doubletipp = true;
                    main.d_group.Children.Remove(main.doubletipp);
                }
            }

            if (mousePosition.X >= 715 && mousePosition.X <= 1205 && mousePosition.Y >= 548 && mousePosition.Y <= 993 && vm.MenuuLogic.Marker == 0)
            {
                // új játék
                if (mousePosition.Y <= 648)
                {
                    PlayerSetup ps = new PlayerSetup();
                    if (ps.ShowDialog() == true)
                    {
                        vm.MenuuLogic.Marker = 1;
                        main.d_group.Children.Clear();
                        ViewModel.Player = ps.NewPlayer;
                        vm.BG.GameScreenElements();
                        vm.TimeElement();
                        vm.Screen.PutGameScreenElements();
                        vm.BG.ResetAnswerBackgroundBrush();
                        vm.Start();
                        Processing.MediaPlayer.Stop();
                        Processing.PlaySong("question_start.mp3");
                    }
                }

                // szabályok
                else if (mousePosition.Y >= 663 && mousePosition.Y <= 763)
                {
                    main.d_group.Children.Remove(main.rules_and_top_players_background);
                    main.d_group.Children.Remove(vm.Screen.TopPlayersText);
                    main.d_group.Children.Remove(vm.Screen.RulesText);
                    vm.BG.RulesTopPlayersRectCreate();
                    main.d_group.Children.Add(main.rules_and_top_players_background);
                    vm.Screen.PrintText(Processing.ReadRulesTextFromFile(), StringContentEnum.Rules);
                }

                // ranglista
                else if (mousePosition.Y >= 778 && mousePosition.Y <= 878)
                {
                    main.d_group.Children.Remove(main.rules_and_top_players_background);
                    main.d_group.Children.Remove(vm.Screen.RulesText);
                    main.d_group.Children.Remove(vm.Screen.TopPlayersText);
                    vm.BG.RulesTopPlayersRectCreate();
                    main.d_group.Children.Add(main.rules_and_top_players_background);
                    vm.Screen.PrintText(Processing.ReadPlayersFromFile(), StringContentEnum.Top);
                }

                // kilépés
                else if (mousePosition.Y >= 893)
                {
                    Processing.MediaPlayer.Stop();
                    main.Close();
                }
            }
            else if (vm.MenuuLogic.Marker == 0)
            {
                main.d_group.Children.Remove(main.rules_and_top_players_background);
                main.d_group.Children.Remove(vm.Screen.TopPlayersText);
                main.d_group.Children.Remove(vm.Screen.RulesText);
            }
        }

        // menü-n belüli klikkek
    }
}
