﻿//-----------------------------------------------------------------------
// <copyright file="EndStar.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LegyenOnIsMilliomos
{
    using System.Globalization;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// A játék végén megjelenő "jutalom" csillag a játékban való szereplésért.
    /// </summary>
    public class EndStar : Bindable
    {
        private Rect star;
        private int size;
        private bool grow;
        private MainWindow main;
        private GeometryDrawing old;

        /// <summary>
        /// Initializes a new instance of the <see cref="EndStar"/> class.
        /// Inicializálja a jutalom csillagot a játék végén.
        /// </summary>
        /// <param name="main">Hivatkozás a main windowra.</param>
        public EndStar(MainWindow main)
        {
            this.size = 1080;
            this.grow = false;
            this.main = main;
            this.Opc("star");
        }

        /// <summary>
        /// Gets or sets csillag téglalpjának tulajdonsága.
        /// </summary>
        public Rect Star
        {
            get
            {
                return this.star;
            }

            set
            {
                this.star = value;
                this.Opc("star");
            }
        }

        /// <summary>
        /// Gets Csillag képét tölti be.
        /// </summary>
        public Brush StarBrush
        {
            get
            {
                return Processing.GetBrush("picture/star.png");
            }
        }

        /// <summary>
        /// A csilaggban a megfelelő pozivióba helyezi a játékos nyereményét
        /// </summary>
        /// <param name="money">A megnyert összeget tartalmazza</param>
        public void PrintText(string money)
        {
            Geometry geometry1;
            Geometry geometry2;
            int font_height = (int)this.star.Width / 15;
            if (font_height == 0)
            {
                font_height = 1;
            }

            FormattedText text1 = new FormattedText("Nyereménye:", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), font_height, Brushes.Black); // kérdés első fele
            geometry1 = text1.BuildGeometry(new Point((1920 - text1.WidthIncludingTrailingWhitespace) / 2, (1080 - (font_height * 3)) / 2));
            FormattedText text2 = new FormattedText(money, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Tahoma"), font_height, Brushes.Black); // kérdés második fele
            geometry2 = text2.BuildGeometry(new Point((1920 - text2.WidthIncludingTrailingWhitespace) / 2, (1080 - font_height) / 2));

            GeometryDrawing g1 = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry1);
            GeometryDrawing g2 = new GeometryDrawing(new SolidColorBrush(Colors.White), null, geometry2);
            if (this.old == null)
            {
                this.old = g1;
            }
            else
            {
                this.main.d_group.Children.RemoveAt(this.main.d_group.Children.Count - 1);
                this.main.d_group.Children.RemoveAt(this.main.d_group.Children.Count - 1);
            }

            this.main.d_group.Children.Add(g1);
            this.main.d_group.Children.Add(g2);
        }

        /// <summary>
        /// A csillagot zsugorítja és növeli meg.
        /// </summary>
        /// <param name="decrease">Az az érték amivel a csillag éppen csökkent (vagy nő)</param>
        public void Animate(int decrease)
        {
            if (this.grow)
            {
                this.size += decrease;
                if (this.size >= 1080)
                {
                    this.grow = false;
                }
            }
            else
            {
                this.size -= decrease;
                if (this.size < 0)
                {
                    this.grow = true;
                }
            }

            if (this.grow)
            {
                this.size += decrease;
                if (this.size >= 1080)
                {
                    this.grow = false;
                }
            }

            this.star = new Rect((1920 - this.size) / 2, (1080 - this.size) / 2, this.size, this.size);
            this.Opc("star");
        }
    }
}